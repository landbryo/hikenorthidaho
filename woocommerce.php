<?php get_header(); ?>			
		
	<div class="col-md-12 single">
	
		<div class="<?php if( get_theme_mod( 'fullby_sidebar_wc' ) == '0') { ?> col-md-9 <?php } else { ?> col-md-12 <?php } ?>">

			<div class="wc-content">
				
				<?php woocommerce_content(); ?>

			</div>	

		</div>	
		 
		<?php if( get_theme_mod( 'fullby_sidebar_wc', '0' ) == '0') { ?>
		 
		<div class="col-md-3">
		
			<div class="sec-sidebar">

				<?php get_sidebar( 'woocommerce' ); ?>	
										
		    </div>
		   
		 </div>
		 
		 <?php } ?>

	</div>			
		
<?php get_footer(); ?>