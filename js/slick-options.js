(function ($) {	
	
	/* Variables from php */
	
	var autoplay = php_vars.autoplay == 'true' ? true : false ;
	
	var pause_hover = php_vars.pause_hover == 'true' ? true : false ;
	
	var autoplay_speed = php_vars.autoplay_speed;
	
			
	/* Slick Slider */

	$('.cont-slick-slider').slick({
	  infinite: true,
	  slidesToShow: 4,
	  slidesToScroll: 1,
	  autoplay: autoplay,
	  pauseOnHover: pause_hover,
      autoplaySpeed: autoplay_speed,
	  slide: '.item-featured',
	  responsive: [
	    {
	      breakpoint: 1024,
	      settings: {
	        slidesToShow: 3,
	        slidesToScroll: 3,
	        infinite: true,
	        dots: true
	      }
	    },
	    {
	      breakpoint: 600,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 2
	      }
	    },
	    {
	      breakpoint: 480,
	      settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1
	      }
	    }
	  ]
	});
	
	// Open Slick Slider on Load Images 
	
	$('.cont-slick-slider').addClass('open-slick');
	
	
			
}(jQuery));
