<?php get_header(); ?>	

	<?php
		
		$main_sidebar_right = get_theme_mod( 'fullby_col_setting', 'option1' );
		$main_sidebar_hide = get_theme_mod( 'fullby_sidebar1_single', '0' ); 
		$second_sidebar_hide = get_theme_mod( 'fullby_sidebar2', '0' ); 
			
	?>		
		
	<div class="<?php if ( $main_sidebar_hide  == '1') { ?> col-md-12 <?php } else { ?> col-md-9 <?php } ?> <?php if ( $main_sidebar_right == 'option1'  && $main_sidebar_hide  != '1') { ?> col-md-push-3 <?php } ?> single">
	
		<div class="<?php if( $second_sidebar_hide == '0') { ?> col-md-9 <?php } else { ?> col-md-12 <?php } ?> no-margin">
		
			<?php if (have_posts()) :?><?php while(have_posts()) : the_post(); ?> 

				<?php if ( has_post_thumbnail() ) { ?>

                    <?php the_post_thumbnail('single', array('class' => 'sing-cop')); ?>

                <?php } else { ?>
                
                	<div class="row spacer-sing"></div>	
                
                <?php }  ?>
                
                <div class="single-in">
				
					<div class="sing-tit-cont">
						
						<h2 class="sing-tit"><?php the_title(); ?></h2>
					
					</div>
	
					<div class="sing-cont">
						
						<div class="sing-spacer">
						
							<?php the_content('Leggi...');?>
	
						</div>
	
					</div>	
				
				</div><!--/single-in-->
				 					
			<?php endwhile; ?>
	        <?php else : ?>

	                <p><?php _e('Sorry, no posts matched your criteria.', 'fullby'); ?></p>
	         
	        <?php endif; ?> 
	        
		</div>	
		 
		<?php if( $second_sidebar_hide == '0') { ?>
		 
			<div class="col-md-3">
			
				<div class="sec-sidebar">
				
				<?php
				
				// if woocomerce is active
				
				if ( class_exists( 'WooCommerce' ) ) {
					
					// if is woocomerce page
					
					if ( is_cart() || is_checkout() ) {
						
						 get_sidebar( 'woocommerce' );
		
					} 
				  
				} else {
				  
				  get_sidebar( 'secondary' );
				  
				}
				?>	
											
			    </div>
			   
			 </div>
		 
		 <?php } ?>

	</div>	
	
	<?php if ( $main_sidebar_hide  != '1') { ?>			

		<div class="col-md-3 <?php if ( $main_sidebar_right == 'option1' ) { ?> col-md-pull-9 <?php } ?> sidebar">
	
			<?php get_sidebar( 'primary' ); ?>	
			    
		</div>
	
	<?php } ?> 
		
<?php get_footer(); ?>