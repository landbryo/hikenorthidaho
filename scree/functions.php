<?php

// SCREE ENQUEUE SCRIPTS

function ScreeEnqueueScripts() {
	wp_enqueue_style( 'map-style', get_template_directory_uri() . '/scree/css/map.css' );
	wp_enqueue_style( 'scree-custom-style', get_template_directory_uri() . '/scree/css/custom.css' );
	wp_enqueue_style( 'scree-mobile-style', get_template_directory_uri() . '/scree/css/mobile.css' );

	wp_enqueue_script( 'google-map-script', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDfXFoscYDFcGHaB4vsdI8GRsUpJ36nhXM' );

	wp_enqueue_script( 'map-script', get_template_directory_uri() . '/scree/js/map.js' );
}

add_action( 'wp_enqueue_scripts', 'ScreeEnqueueScripts' );

// SCREE DASHBOARD SCRIPTS

function my_custom_fonts() {
	wp_enqueue_style( 'scree-dashboard-style', get_template_directory_uri() . '/scree/css/dashboard.css' );
}

add_action( 'admin_head', 'my_custom_fonts' );

// ADD FEATURED IMAGE TO RESTAPI OUTPUT

function hni_insert_thumbnail_url() {
	register_rest_field( 'post',
		'hni_thumbnail',
		array(
			'get_callback'    => 'hni_get_thumbnail_url',
			'update_callback' => null,
			'schema'          => null,
		)
	);
}

function hni_get_thumbnail_url( $post ) {
	if ( has_post_thumbnail( $post['id'] ) ) {
		$imgArray = wp_get_attachment_image_src( get_post_thumbnail_id( $post['id'] ), 'thumbnail' );
		$imgURL   = $imgArray[0];

		return $imgURL;
	} else {
		return false;
	}
}

add_action( 'rest_api_init', 'hni_insert_thumbnail_url' );

// ADD WEATHER WIDGET

function scree_weather() { ?>

    <a style="position: relative;width: 100%;height: 98px;" class="weatherwidget-io"
       href="https://forecast7.com/en/48d28n116d55/sandpoint/?unit=us" data-label_1="NORTH IDAHO" data-label_2="WEATHER"
       data-theme="dark">SANDPOINT WEATHER</a>
    <script>
        !function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (!d.getElementById(id)) {
                js = d.createElement(s);
                js.id = id;
                js.src = 'https://weatherwidget.io/js/widget.min.js';
                fjs.parentNode.insertBefore(js, fjs);
            }
        }(document, 'script', 'weatherwidget-io-js');
    </script>

<?php }

add_action( 'scree_pre_footer', 'scree_weather' );

/**
 * Add Google Map API key for ACF
 */

function scree_map_api() {

    update_option('google_api_key', 'AIzaSyDfXFoscYDFcGHaB4vsdI8GRsUpJ36nhXM' );

}

add_action( 'init', 'scree_map_api' );