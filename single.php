<?php get_header(); ?>

<?php

$big_image_grid = get_theme_mod( 'fullby_activate_big_grid', '0' );

$main_sidebar_right  = get_theme_mod( 'fullby_col_setting', 'option1' );
$main_sidebar_hide   = get_theme_mod( 'fullby_sidebar1_single', '0' );
$second_sidebar_hide = get_theme_mod( 'fullby_sidebar2', '0' );

?>

    <div class="<?php if ( $main_sidebar_hide == '1' ) { ?> col-md-12 <?php } else { ?> col-md-9 <?php } ?> <?php if ( $main_sidebar_right == 'option1' && $main_sidebar_hide != '1' ) { ?> col-md-push-3 <?php } ?> single">

        <div class="<?php if ( $second_sidebar_hide == '0' ) { ?> col-md-9 <?php } else { ?> col-md-12 <?php } ?> no-margin <?php if ( $big_image_grid == '1' ) { ?> big-images-grid-single <?php } ?>">

			<?php if ( have_posts() ) : ?><?php while ( have_posts() ) : the_post(); ?>

                <div class="single-in">

					<?php // embed Video

					$video = get_post_meta( $post->ID, 'fullby_video', true );

					if ( $video != '' ) {

						// if function video is enable
						if ( function_exists( 'video_player' ) ) {

							?>

                            <div class="videoWrapper">

                                <div class='video-container'><?php echo video_player( $video ); ?></div>

                            </div>

							<?php

						}

					} else if ( has_post_thumbnail() ) {

						the_post_thumbnail( 'large', array( 'class' => 'sing-cop' ) );

					} else { ?>

                        <div class="row spacer-sing"></div>

					<?php } ?>


                    <div class="sing-tit-cont <?php if ( $video != '' ) { ?> video-sing-post <?php } ?>">

                        <p class="cat"> <?php the_category( ', ' ); ?></p>

                        <h2 class="sing-tit"><?php the_title(); ?></h2>

                        <p class="meta">

                            <i class="fa fa-clock-o"></i> <?php the_time( 'j M , Y' ) ?> &nbsp;

							<?php // icon Video

							if ( ( $video != '' ) ) { ?>

                                <i class="fa fa-video-camera"></i> Video

							<?php } else if ( strpos( $post->post_content, '[gallery' ) !== false ) { ?>

                                <i class="fa fa-th"></i> Gallery

							<?php } else { ?>

							<?php } ?>

                        </p>

                    </div>

                    <div class="sing-cont">

                        <div class="sing-spacer">

							<?php the_content( 'Leggi...' ); ?>

							<?php $gallery_image_ids = get_field( 'submission_gallery', false, false );
							$gallery_shortcode       = '[gallery ids="' . implode( ',', (array) $gallery_image_ids ) . ' link="file"]';
							echo do_shortcode( $gallery_shortcode ); ?>

							<?php wp_link_pages( 'pagelink=Page %' ); ?>

							<?php $hike_location = get_field( 'hike_location' );
							if ( ! empty( $hike_location ) ): ?>
                                <iframe style="width: 100%; height: 400px; border: none;"
                                        src="https://www.google.com/maps/embed/v1/place?key=<?php echo get_option( ' google_api_key' ) ?>&q=<?php echo urlencode( $hike_location ); ?>&zoom=12"></iframe>
							<?php endif; ?>

                            <p class="cont-tag">
								<?php $post_tags = wp_get_post_tags( $post->ID );
								if ( ! empty( $post_tags ) ) { ?>

                                    <span class="tag-post"> <i class="fa fa-tag"></i> <?php the_tags( '', ', ', '' ); ?></span>

								<?php } ?>
                            </p>

							<?php // social share

							if ( get_theme_mod( 'fullby_share_content', '1' ) == '1' ) { ?>

								<?php get_template_part( 'sharrre' ); ?>

							<?php } ?>

							<?php // related posts

							if ( get_theme_mod( 'fullby_author', '0' ) == '0' ) {

								$desc_author = get_the_author_meta( 'description' ); ?>

                                <p class="cont-author <?php if ( $desc_author == '' ) {
									echo 'no-desc';
								} ?>">
									<?php echo get_avatar( get_the_author_meta( 'user_email' ), 70 ); ?>

                                    <strong class="name-author"><?php _e( 'By ' ); ?><?php the_author_posts_link(); ?></strong>

									<?php $user_twitter_link = get_the_author_meta( 'twitter' );
									if ( $user_twitter_link != '' ) { ?> &nbsp;- &nbsp; <a
                                            href="<?php echo $user_twitter_link; ?>"><i
                                                class="fa fa-twitter-square"></i></a> <?php } ?>
									<?php $user_facebook_link = get_the_author_meta( 'facebook' );
									if ( $user_facebook_link != '' ) { ?> &nbsp; <a
                                            href="<?php echo $user_facebook_link; ?>"><i
                                                class="fa fa-facebook-square"></i></a> <?php } ?>
									<?php $user_googleplus_link = get_the_author_meta( 'googleplus' );
									if ( $user_googleplus_link != '' ) { ?> &nbsp; <a
                                            href="<?php echo $user_googleplus_link; ?>"><i
                                                class="fa fa-google-plus-square"></i></a> <?php } ?>
									<?php $user_linkedin_link = get_the_author_meta( 'linkedin' );
									if ( $user_linkedin_link != '' ) { ?> &nbsp; <a
                                            href="<?php echo $user_linkedin_link; ?>"><i
                                                class="fa fa-linkedin-square"></i></a> <?php } ?>
									<?php $user_youtube_link = get_the_author_meta( 'youtube' );
									if ( $user_youtube_link != '' ) { ?> &nbsp; <a
                                            href="<?php echo $user_youtube_link; ?>"><i
                                                class="fa fa-youtube-square"></i></a> <?php } ?>

                                    <br/>

									<?php echo $desc_author; ?>
                                </p>

							<?php } ?>


							<?php // related posts

							if ( get_theme_mod( 'fullby_related_posts', '0' ) == '0' ) { ?>

								<?php get_template_part( 'inc/related_posts' ); ?>

							<?php } ?>


                            <div class="clear"></div>

                            <hr/>

                            <div id="comments">

								<?php comments_template(); ?>

                            </div>

                        </div>

                    </div>

                    <div class="clearfix"></div>

                </div><!--/single-in-->

			<?php endwhile; ?>
			<?php else : ?>

                <p><?php _e( 'Sorry, no posts matched your criteria.', 'fullby' ); ?></p>

			<?php endif; ?>

        </div>

		<?php if ( $second_sidebar_hide == '0' ) { ?>

            <div class="col-md-3">

                <div class="sec-sidebar">

					<?php get_sidebar( 'secondary' ); ?>

                </div>

            </div>

		<?php } ?>

    </div>

<?php if ( $main_sidebar_hide != '1' ) { ?>

    <div class="col-md-3 <?php if ( $main_sidebar_right == 'option1' ) { ?> col-md-pull-9 <?php } ?> sidebar">

		<?php get_sidebar( 'primary' ); ?>

    </div>

<?php } ?>


<?php get_footer(); ?>