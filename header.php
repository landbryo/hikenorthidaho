<!DOCTYPE html>
<html  <?php language_attributes();?>>
  <head>
    <meta charset="utf-8">
    
    <title><?php wp_title('&raquo;','true','right'); ?><?php bloginfo('name'); ?></title>
    
    
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- Favicon -->
    
    <link rel="icon" href="<?php echo get_theme_mod( 'fullby_favicon', get_stylesheet_directory_uri(). '/img/favicon.png' ); ?>" type="image/x-icon">
	
	
	
    <!-- Meta for IE support -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge"> 
    
    <!-- Analytics -->
    <?php if ( get_theme_mod( 'fullby_analytics' ) ) { ?>
    
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		
		  ga('create', '<?php echo get_theme_mod( 'fullby_analytics' ); ?>', '<?php echo get_theme_mod( 'fullby_analytics_dom' ); ?>');
		  ga('send', 'pageview');
		
		</script>
		
	<?php } ?>

	<?php wp_head(); ?>
	
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
      
      <style> /* style for fix ie */ 
      
      	.item-featured-single {margin-top:50px}
      	.bg-list{top:0;}
      	
      </style>
      
    <![endif]--> 
	
</head>
<body <?php body_class(); ?>>
<?php if ( function_exists( 'gtm4wp_the_gtm_tag' ) ) { gtm4wp_the_gtm_tag(); } ?>

    <div class="navbar navbar-inverse navbar-fixed-top">
     
		<div class="navbar-header">
		  
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#mainmenu">
			
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				
			</button>
			
			<?php if ( get_theme_mod( 'fullby_logo' ) ) { ?>
			
				<a class="logo-img" href="<?php echo esc_url( home_url( '/' ) ); ?>" id="site-logo" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
				
				    <img src="<?php echo get_theme_mod( 'fullby_logo' ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
				
				</a>
				
				<h1 class="seo-title"><?php bloginfo('name'); ?></h1>
			
			<?php } else { ?>
			           
				<h1><a class="navbar-brand" href="<?php echo home_url(); ?>"><?php echo get_theme_mod( 'fullby_icon' , '<i class="fa fa-chevron-right"></i><i class="fa fa-chevron-right"></i>'); ?>&nbsp; <span><?php bloginfo('name'); ?></span></a><small><?php bloginfo('description'); ?></small> </h1>
					               
			<?php } ?>
		  
		</div>
		
		<div id="mainmenu" class="collapse navbar-collapse">
		
			<?php /* Primary navigation */
				wp_nav_menu( array(
				  'theme_location' => 'primary',
				  'depth' => 2,
				  'container' => false,
				  'menu_class' => 'nav navbar-nav navbar-right',
				  'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
				  //Process nav menu using our custom nav walker
				  'walker' => new wp_bootstrap_navwalker())
				);
				
				
			?>
			
		</div><!--/.nav-collapse -->
    
    </div>
    
    <?php if (is_front_page()) { ?>
    
    	 <?php if (!is_paged()){ ?> 
    	 
    	 	 <?php // Cover Home
	    	 	 
	    	$cover_set = get_theme_mod( 'fullby_cover_content', 'option0' ) ;
    	 	
    	 	 // if cover selected in backend 
    	 	 	 	
    	 	 if ( 'option1' == $cover_set ) { 
    	 	 
    	 	 		// include header video
    	 	 		
    	 	 		get_template_part( 'inc/header', 'video' );  
    	 	 		
    	 	 		
    	 	 } else if ( 'option2' == $cover_set ) { 


    	 	 		// include header video autoplay
    	 	 		
    	 	 		get_template_part( 'inc/header', 'video-autoplay' );  
    	 	 		
	 
    	 	 } else if ( 'option3' == $cover_set ) { 
    	 	 
    	 	 		    	 	 		
    	 	 		 // include header 3 columns
    	 	 		
    	 	 		get_template_part( 'inc/header', '3-columns' ); 
    	 	 		
	    	 
    	 	 } else if ( 'option4' == $cover_set ) { 	
    	 	 		
    	 	 		
    	 	 		// slider video
    	 	 		
    	 	 		get_template_part( 'inc/header', 'slider' ); 

    	 	 			
    	 	 } else if ( 'option5' == $cover_set ) { 	
    	 	 		
    	 	 		// touch slider
    	 	 		
    	 	 		get_template_part( 'inc/header', '4-touch-slider' ); 
    	 	 		
    	 	 } else if ( 'option6' == $cover_set ) { 	
    	 	 		
    	 	 		// single image + list
    	 	 		    	 	 		
    	 	 		get_template_part( 'inc/header', 'single-image-list' ); 
    	 	 		
    	 	 } else if ( 'option7' == $cover_set ) { 	
    	 	 		
    	 	 		// single image + list
    	 	 		    	 	 		
    	 	 		get_template_part( 'inc/header', '4-box-grid' ); 
    	 	 		
    	 	 		
    	 	 } else if ( 'option0' == $cover_set ) { 	
	    	 	 
	    	 	 
	    	 	 // don't show any header
	    	 	 
	    	 	 echo '<div class="spacer"></div>';

    	 	 			
    	 	 } // end if cover selected 
    	 	 
    	 	 ?>
			
				
		<?php } else { ?>
		
			<div class="row spacer"></div>	
		
		<?php } // end if (!is_paged) ?>
				
	<?php } else { ?>	
	
		<div class="row spacer"></div>		   
			
	<?php  } // end if (is_home) ?>
		
	
	<div class="navbar navbar-inverse navbar-sub">
     
		<div class="navbar-header navbar-header-sub">
			
			<?php if( get_theme_mod( 'fullby_menu2', '0' ) == '0') { ?> 
		
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#submenu">
				
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					
				</button>
			
			<?php } ?>
			
			<?php if (is_front_page()) { // if is home show button widget bar  ?>
				
				<?php if (!is_paged()){ // if is page 2 or more hide button widget bar ?> 
				
					<?php // show/hide button widget bar from theme customizer

					if( get_theme_mod( 'fullby_show_widget_bar' ) == '1') { ?>
				
						<div class="widget-bar-button"> <i class="fa fa-angle-up fa-3x"></i> </div>
						
					<?php } ?>		
				
				<?php } ?>
			
			<?php } ?>
		
		</div>
		
		<?php if( get_theme_mod( 'fullby_menu2', '0' ) == '0') { ?> 
		
			<div  id="submenu" class="collapse navbar-collapse">
				
				<?php /* Secondary navigation */
					wp_nav_menu( array(
					  'theme_location' => 'secondary',
					  'depth' => 2,
					  'container' => false,
					  'menu_class' => 'nav navbar-nav',
					  'fallback_cb' => 'wp_bootstrap_navwalker::fallback',
					  //Process nav menu using our custom nav walker
					  'walker' => new wp_bootstrap_navwalker())
					);
				?>
			
				<div class="col-sm-3 col-md-3 pull-right search-cont">
				    <form class="navbar-form" role="search" method="get" action="<?php echo home_url() ; ?>">
				        
				        <div class="input-group">
				            <input type="text" class="form-control" placeholder="Search" name="s" id="srch-term">
				            <div class="input-group-btn">
				                <button class="btn btn-default" type="submit"></button>
				            </div>
				        </div>
				    </form>
				</div>
						
			</div><!--/.nav-collapse -->
		
		<?php } ?>

	</div>