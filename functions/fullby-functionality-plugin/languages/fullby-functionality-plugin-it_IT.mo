��          �      �       0     1  +   L  )   x  F   �     �     �          	          *  p   1  }   �  �     !   �  3     /   :  B   j     �     �     �     �     �     �  Z   �  c   S         
                      	                           Choose a category to show: Display latest 3 post from custom category. Display latest post from custom category. Display popular and latest post in a user friendly tab with thumbnail. Last 1 post Last 3 posts Latest Popular Popular and Latest Posts Title: To set an <strong>Image as a Preview</strong> of the video use the <strong>Featured Image</strong> oh the right. To show a video in the article paste the link of a <strong>YouTube</strong> or <strong>Vimeo</strong> video in the box below. Project-Id-Version: Fullby Functionality Plugin
POT-Creation-Date: 2015-01-17 15:28+0100
PO-Revision-Date: 2015-01-17 15:28+0100
Last-Translator: Andrea Marchetti <afmarchetti@gmail.com>
Language-Team: 
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.6.7
X-Poedit-Basepath: .
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: _e;__
X-Poedit-SearchPath-0: ..
 Scegli una categoria da mostrare. Mostra gli ultimi 3 post da una categoria a scelta. Mostra l'ultimo post da una categoria a scelta. Mostra i post più popolari e recenti in una tab con le thumbnail. Ultimo Post Ultimi 3 post Recenti Popolari Post Popolari e Recenti Titolo: Per impostare una immagine come preview del video usa l'immagine in evidenza sulla destra. Per mostrare un video nell'articolo incolla l'url della pagina Youtube o Vimeo nel box sottostante. 