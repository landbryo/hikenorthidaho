��    L      |  e   �      p     q  
   }     �     �     �     �     �     �  )   �  <        W     i     ~     �     �     �     �     �     �     �     �     �     	          $     1  !   L     n     �  !   �     �     �     �  
   �     �     	     	     	     0	     G	     X	     e	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	  %   
  "   4
  !   W
     y
     �
     �
     �
  &   �
     �
          
       &   &     M     \  E   u     �     �  L   �  h   -     �  �   �     -  �  :  
   �  
   
          $     ;     R  !   _     �  7   �  ;   �     �          #     ,     :     R     a     n     |     �     �     �     �     �     �  $   �  ,        8     W      o     �     �     �     �     �     �          	          2     C      S     t     �     �     �     �     �     �     �     �     �  2         D  6   e     �     �     �     �  &   �          6     <     Q  6   X     �  )   �  Q   �          3  J   @  f   �     �  �        �            >   ;         F            &   G      7   /                  :   K       6   +   A   $             J   ,          
      @          D         (   *                             8   .       5   L   #      %   I      C       <   '       )   2                                    -   	      4      3   B          0       ?             "       9      !   E   H   1   =    % Responses 1 Response 3 columns Grid 4 Columns Touch Slider 404 Page not found Analytics ID Autoplay Speed (milliseconds) By  Change how Fullby display Social Elements Change how Fullby displays Header Covers, Posts and Sidebars Comments RSS Feed Comments are closed. Excerpts Facebook Link Favicon 16 x 16 px .png Featured Color Featured Tag Fixed height Font Full content Google Font Setting Google+ Link Header Color Header Height Header Style Hide Author on Single Page Hide Related Posts in Single Page Hide Secondary Sidebar Horizontal Widget Bar Icon - Paste it from Font Awesome Instagram Link Layout Options Letter Case Setting Link Color Link Hover Color Linkedin Link Logo Logo & Menu Color Logo, Favicon & Header Meta Description No Responses Pause on Hover Touch Slider Pinterest Link Post Content Post by: Primary Sidebar Primary navigation Related posts Result for: SEO Secondary Sidebar Secondary navigation Select Font for Logo, Title and Menu. Show Horizontal Widget Bar in Home Show share buttons on single page Sidebar Left Sidebar Position Sidebar Right Single Parallax Image / Video Single Parallax Image / Video Autoplay Site Domain (without www) Skins Slider Image / Video Social Sorry, no posts matched your criteria. Submit Comment Tag use for Header Posts This post is password protected. Enter the password to view comments. Try to make a search... Twitter Link Upload a logo to replace the default site name and description in the header Use the "featured" tag to feature your posts in the header. You can change this to a tag of your choice; Woocommmerce Sidebar Write a Meta Description. To activate Google Analytics insert Analytics ID code and the site domain without www (ex. mysite.com). YouTube Link Project-Id-Version: Fullby
POT-Creation-Date: 2015-01-30 10:21+0100
PO-Revision-Date: 2015-01-30 10:24+0100
Last-Translator: Andrea Marchetti <afmarchetti@gmail.com>
Language-Team: 
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.6.7
X-Poedit-Basepath: .
Plural-Forms: nplurals=2; plural=n != 1;
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c
X-Poedit-SearchPath-0: ..
 % Commenti 1 Commento 3 columns Grid 4 Colonne Touch Slider 404 Pagina non trovata Analytics ID Velocità Autoplay (millisecondi) Di: Cambia il modo in cui fullby mostra gli elementi social Cambia il modo in cui fullby mostra Header, sidebar e post. Feed Rss Commenti I commenti sono chiusi. Estratto Facebook Link Favicon 16 x 16 px .png Featured Color Featured Tag Altezza Fissa Font Tutto il contenuto Settaggi Google Font  Google+ Link Colore header Altezza Header Stile dell'header Nascondi autore nella pagina singola Nascondi post correlati nel articolo singolo Nascondi la sidebar secondaria Widget Bar Orizzontale  Icona - icollala da Font Awesome Instagram Link Opzioni Layout Settaggia Maiuscolo/Minuscolo Colore dei link Colore all'hover dei link Linkedin Link Logo Logo & Menu Color Logo, Favicon & Header Meta Description Nessun Commento Pausa all'hover del Touch Slider Pinterest Link Contenuto del post Post di: Sidebar Primaria Navigazione Primaria Post Correlati Risultati per: SEO Sidebar Secondaria Navigazione Secondaria Seleziona il font per il logo, i titoli e il menu. Mostra barra orizzontale in home Mostra i pulsanti di condivisione sul articolo singolo Sidebar a sinistra Posizione della sidebar Sidebar a destra Single Parallax Image / Video Single Parallax Image / Video Autoplay Dominio Sito (senza www) Skins Slider Image / Video Social Nessun contenuto trovato per questa chiave di ricerca. Invia Commento Tag usato per mostrare i post nell'header Questo post è protetto da password. Inserisci la password per vedere i commenti. Prova con una ricerca... Twitter Link Carica un logo per sostituire il nome visualizzato di default nell'header. Usa il "featured" tag per most rare i post nell'header. Puoi cambiare questo tag con uno a tua scelta. Woocommerce Sidebar  Scrivi una meta description. Per attivare google Analitics, inserisci l'ID di google analytics e il dominio del sito senza il www. (ex. mysite.com). YouTube Link 