<?php get_header(); ?>			
		
	<div class="col-md-12 single">
	
		<div class="<?php if( get_theme_mod( 'fullby_sidebar_wc' ) == '0') { ?> col-md-9 <?php } else { ?> col-md-12 <?php } ?>">

				
			<?php if (have_posts()) :?><?php while(have_posts()) : the_post(); ?> 

				<div class="wc-content">          
               	
					<h2 class="sing-tit"><?php the_title(); ?></h2>

					<?php the_content('Leggi...');?>

				</div>

				 					
			<?php endwhile; ?>
	        <?php else : ?>

	                <p><?php _e('Sorry, no posts matched your criteria.', 'fullby'); ?></p>
	         
	        <?php endif; ?> 

		</div>	
		 
		<?php if( get_theme_mod( 'fullby_sidebar_wc', '0' ) == '0') { ?>
		 
			<div class="col-md-3">
			
				<div class="sec-sidebar">
	
					<?php get_sidebar( 'woocommerce' ); ?>	
											
			    </div>
			   
			 </div>
		 
		 <?php } ?>

	</div>			
		
<?php get_footer(); ?>