<div class="featured-bar">
	
	<?php $tag = get_theme_mod( 'fullby_featured' ); ?>
	
	<strong class="featured-item"><?php echo $tag ?>:</strong> 
	
	<?php //loop featured bar
	
	$specialPosts = new WP_Query();
	$specialPosts->query('tag=featured&showposts=3&offset=1');
	
	?>
	
	<?php if ($specialPosts->have_posts()) : while($specialPosts->have_posts()) : $specialPosts->the_post(); ?>
		
		<?php $video = get_post_meta($post->ID, 'fullby_video', true );	?>
		
		<a href="<?php the_permalink(); ?>">
		
			<?php if(($video != '')) { ?>
         			
         		<i class="fa fa-video-camera"></i>
         			
         	<?php } else if (strpos($post->post_content,'[gallery') !== false) { ?>
         			
         		<i class="fa fa-th"></i>

     		<?php } else {?>
     			
     			<i class="fa fa-thumb-tack"></i>

     		<?php } ?>
		
     		<?php the_title(); ?>
		
		</a>
		
	<?php endwhile;  else :  endif; ?>	
	
</div>
