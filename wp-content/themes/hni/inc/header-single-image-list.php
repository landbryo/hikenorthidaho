<?php $tag = get_theme_mod( 'fullby_featured', 'featured' ); ?>

    
<?php // Loop Single Cover 

$specialPosts = new WP_Query();
$specialPosts->query('tag='.$tag.'&showposts=1');

?>

<?php if ($specialPosts->have_posts()) : while($specialPosts->have_posts()) : $specialPosts->the_post(); ?>

	<?php $video = get_post_meta($post->ID, 'fullby_video', true );	?>

	
	<?php // Cover Image
	
	// if post thumbnail exist
	
	if ( has_post_thumbnail() ) { ?>
	
			
			<div class="item-featured-single single-image-list" id="cover" style="background: url(<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'cover' ); ?>); background-size:100% auto; background-attachment:fixed;">
	
			    <a href="<?php the_permalink(); ?>" class="link-featured-single">
	
			    	<div class="caption">
			    	
			    		<div class="cat"><span><?php $category = get_the_category(); echo $category[0]->cat_name; ?></span></div>
			    		
			    		<div class="date-feat"><i class="fa fa-clock-o"></i> <?php the_time('j M , Y') ?> &nbsp;
			    		
	
							<?php if(($video != '')) { ?>
			             			
			             		<i class="fa fa-video-camera"></i> Video
			             			
			             	<?php } else if (strpos($post->post_content,'[gallery') !== false) { ?>
			             			
			             		<i class="fa fa-th"></i> Gallery
	
		             		<?php } else {?>
	
		             		<?php } ?>
	
			    		
			    		</div>
			    		
			    		<h2 class="title"><?php the_title(); ?></h2>
			    		
		    		</div>
		    		
		    	</a>
		    	
		    	<div class="bg-list"></div>
		    	
				<?php get_sidebar( 'widget-bar' ); ?>
				

			    	
	    	</div>			
			
		
		<?php 
	 
	 	} else { // Cover Video 
		 	
		   // if functionality plug in with function video is enable
		   	
		   if( function_exists('video_player')) {
		 
		?>
		
			<div class="item-featured-single" id="cover">
				
				<div class="videoWrapper v-home">
			
				 	<div class='video-container'><?php echo video_player($video); ?></div>
				
				</div>

				<a href="<?php the_permalink(); ?>" class="link-featured-video">
					
					<div class="caption">
					
			    		<div class="cat"><span><?php $category = get_the_category(); echo $category[0]->cat_name; ?></span></div>
			    		
			    		<div class="date-feat"><i class="fa fa-clock-o"></i> <?php the_time('j M , Y') ?> &nbsp;
			    		
			
							<?php if(($video != '')) { ?>
			             			
			             		<i class="fa fa-video-camera"></i> Video
			             			
			             	<?php } else if (strpos($post->post_content,'[gallery') !== false) { ?>
			             			
			             		<i class="fa fa-th"></i> Gallery
			
			         		<?php } else {?>
			
			         		<?php } ?>
			
			    		
			    		</div>
			    		
			    		<h2 class="title"><?php the_title(); ?></h2>
			    		
					</div>
			    	
			    </a>
			    
			    <?php get_sidebar( 'widget-bar' ); ?>
			    						
			</div>			
			    	
	
		 <?php 
		 	
		 	} //end if video is enable
		 
		 
		 } //end if/else post thumbnail exist 
		 
		 ?>

<?php endwhile;  else : ?>

	<div class="row featured">
	
		<p><?php _e('Sorry, no posts matched your criteria.', 'fullby'); ?></p>
		
	</div>

<?php endif; ?>


<div class="list-article-header">
			    	
	

	<?php //loop featured bar
	
	$specialPosts = new WP_Query();
	$specialPosts->query('tag=featured&showposts=3&offset=1');
	
	?>
	
	<?php if ($specialPosts->have_posts()) : while($specialPosts->have_posts()) : $specialPosts->the_post(); ?>
		
		<?php $video = get_post_meta($post->ID, 'fullby_video', true );	?>
		
		<a href="<?php the_permalink(); ?>">
			
			<span>
			
				<?php $category = get_the_category(); echo $category[0]->cat_name; ?> &nbsp;
		
				<?php if(($video != '')) { ?>
	         			
	         		<i class="fa fa-video-camera"></i>
	         			
	         	<?php } else if (strpos($post->post_content,'[gallery') !== false) { ?>
	         			
	         		<i class="fa fa-th"></i>
	
	     		<?php } ?>
     		
			</span>
		
     		<?php the_title(); ?>
		
		</a>
		
	<?php endwhile;  else :  endif; ?>	

	
	
</div>
		    		



