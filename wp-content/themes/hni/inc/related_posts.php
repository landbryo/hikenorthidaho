<?php

$orig_post = $post;
global $post;

$tags = wp_get_post_tags( $post->ID );

if ( $tags ) { ?>

	<div class="related-posts">
	
	<p class="tit-rel"><?php _e( 'Related posts', 'fullby' ); ?></p>
	<div class="row">
		<?php

		$tag_ids = array();
		foreach ( $tags as $individual_tag ) {
			$tag_ids[] = $individual_tag->term_id;
		}

		$args = array(
			'tag__in'             => $tag_ids,
			'post__not_in'        => array( $post->ID ),
			'posts_per_page'      => 3, // Number of related posts to display.
			'ignore_sticky_posts' => 1,
		);

		$my_query = new wp_query( $args );

		while ( $my_query->have_posts() ) {
			$my_query->the_post();
			?>
		
	
		<div class="col-sm-4 col-md-4">
		
			<?php

			$video = get_post_meta( $post->ID, 'fullby_video', true );

			// video preview image

			if ( has_post_thumbnail() ) {
				?>
			
				
				<a class="img-rel-link" href="<?php the_permalink(); ?>">
					<?php the_post_thumbnail( 'small-wide' ); ?>
				</a>


				<?php
			} elseif ( $video != '' ) {

				// if function video is enable

				if ( function_exists( 'video_image_big' ) ) {
					?>
			
				<a class="img-rel-link" href="<?php the_permalink(); ?>">
					<img src="<?php echo video_image_small( $video ); ?>" class="rel-vid"/>
					<i class="fa fa-play-circle fa-4x"></i> 
				</a>
			
					<?php
				}
			}
			?>
			
			<a class="tit-rel-link" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>

		</div>
	
		<?php } ?>
		
	
	
	</div>
		</div>
		
	<hr class="hr"/>
		
	<?php }

$post = $orig_post;
wp_reset_query();

?>
