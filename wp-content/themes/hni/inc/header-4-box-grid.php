<div class="spacer-4-grid-box"></div>

<div class="row featured">

	<?php $tag = get_theme_mod( 'fullby_featured', 'featured' ); ?>
	
	<?php $counter = 0;?>   
    
	<?php // 3 grid post from tag
	
	$specialPosts = new WP_Query();
	$specialPosts->query('tag='.$tag.'&showposts=4');
	
	?>
	
	<?php if ($specialPosts->have_posts()) : while($specialPosts->have_posts()) : $specialPosts->the_post(); ?>
	
	    <div class="grid-box <?php $counter++; ?> grid-box-<?php echo $counter; ?>  item-featured">
	    
			<a href="<?php the_permalink(); ?>">
	
	    		<div class="caption">
	    		
		    		<div class="cat"><span><?php $category = get_the_category(); echo $category[0]->cat_name; ?></span></div>
		    		
		    		<div class="date-feat"><i class="fa fa-clock-o"></i> <?php the_time('j M , Y') ?> &nbsp;
		    		
		    		
		    			<?php //video icon
		    			
						$video = get_post_meta($post->ID, 'fullby_video', true );	
						
						if(($video != '')) { ?>
		             			
		             		<i class="fa fa-video-camera"></i> Video
		             			
		             	<?php } else if (strpos($post->post_content,'[gallery') !== false) { ?>
		             			
		             		<i class="fa fa-th"></i> Gallery
	
	             		<?php } else { ?>
	
	             		<?php } ?>
	
		    		
		    		</div>
		    		
		    		<h2 class="title"><?php the_title(); ?></h2>
		    		
	    		</div>
	    		
	    		
	    		<?php //video preview image
		
				if ( has_post_thumbnail() ) {
					
					if ($counter == 1){
						
						the_post_thumbnail('single', array('class' => 'quad')); 

					} else {
						
						the_post_thumbnail('quad', array('class' => 'quad')); 

					}
						
					 				
				 } else if($video != '') { 
					
					// if functionality plug-in with function video is enable
	
					if( function_exists('video_image_header')) {
	
						echo video_image_header($video); 
			
					}
				
				} ?>
	                	
		    </a>
		
		</div>
	
	<?php endwhile;  else : ?>
	
		<p><?php _e('Sorry, no posts matched your criteria.', 'fullby'); ?></p>
	
	<?php endif; ?>	
	
	<?php get_sidebar( 'widget-bar' ); ?>
	
	
		
</div>	
