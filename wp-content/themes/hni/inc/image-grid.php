<?php 
$post_height = '';

$option_selected = get_theme_mod( 'fullby_post_content', 'option2');

	if ( 'option3' == $option_selected  ) { 
	
		$post_height = 'ok'; 
	
	}	

$video = get_post_meta($post->ID, 'fullby_video', true );
						
// video preview image

if ( has_post_thumbnail() ) { ?>

	
	 <a href="<?php the_permalink(); ?>" class="link-video <?php if ($post_height != ''){?> fixed-height <?php } ?>">
        <?php the_post_thumbnail('medium', array('class' => 'grid-cop')); ?>
        <?php if(($video != '')) { ?>
        	<i class="fa fa-play-circle fa-4x"></i> 
        <?php } ?>
     </a>


<?php } else if($video != '') { 
	
	// if function video is enable
	
	if( function_exists('video_image_big')) {
	
?>

	<a href="<?php the_permalink(); ?>" class="link-video <?php if ($post_height != ''){?> fixed-height <?php }?>">
		<img src="<?php echo video_image_big($video); ?>" class="grid-cop"/>
		<i class="fa fa-play-circle fa-4x"></i> 
	</a>

<?php }


} ?>