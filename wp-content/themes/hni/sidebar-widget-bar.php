<?php // show/hide widget bar

if( get_theme_mod( 'fullby_show_widget_bar', '0' ) == '1') { ?>
	
	<div class="second-button-mobile"> <div class="widget-bar-button"> <i class="fa fa-angle-up fa-3x"></i> </div></div>
	
	<div class="widget-bar">
				    
		<div class="container-widget">
			
			<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar(__('Horizontal Widget Bar', 'fullby')) ) : ?>
		
			<?php endif; ?>
		
		</div>
		
	</div><!--  end widget bar /-->
	
<?php } ?>

