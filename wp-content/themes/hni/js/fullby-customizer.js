jQuery( document ).ready( function( $ ) {

	// Allow real-time updating of the theme customizer
	
	// blog name
	wp.customize( 'blogname', function( value ) {
		value.bind( function( to ) {
			$( '.navbar-brand span' ).html( to );
		} );
	} );
	
	// blog description
	wp.customize( 'blogdescription', function( value ) {
		value.bind( function( to ) {
			$( '.navbar h1 small' ).html( to );
		} );
	} );
	
	// header color
    wp.customize( 'fullby_header_color', function( value ) {
        value.bind( function( to ) {
            $('#mainmenu, .navbar-fixed-top, #submit, .single-image-list .date-feat, .big-images-grid .grid-cat, .big-images-grid-single .cat, .grid-box .date-feat').css('background-color', to );
        });
    });
    
    // link color
    wp.customize( 'fullby_link_color', function( value ) {
        value.bind( function( to ) {
           
            $('a').not('.navbar-inverse a, .grid-tit a, .nav-tabs li.active a, .featured-bar a').css('color', to );
        });
    });
    
	// hover color
	wp.customize( 'fullby_hover_color', function( value ) {
	    value.bind( function( to ) {
	
	        //Hover preview link	
	        $hovercolor = $('.grid-cat a').css("color");
	        
	        $('a').not('.navbar-inverse a, .grid-tit a, .nav-tabs li.active a').hover(
			function () {
					
				$(this).css({
						'color': to,
					});
				},
						function () {
				$(this).css({
						'color': $hovercolor
				
					});
				}
			);
			
	 	 });
	});
	 
	// logo & menu color
	wp.customize( 'fullby_logo_menu_color', function( value ) {
		value.bind( function( to ) {
		  
		    $('.navbar-fixed-top .navbar-nav > li > a, .navbar-fixed-top .navbar-brand, .single-image-list .date-feat, .big-images-grid .grid-cat a, .big-images-grid-single .cat a, .grid-box .date-feat').css('color', to );
		   
		});
	});
	
	// featured color
	wp.customize( 'fullby_featured_color', function( value ) {
	    value.bind( function( to ) {
	      
	        $('.widget-bar-button').css('background-color', to );
	        $('.date-feat, .tag-post, .list-article-header a span').css('color', to );
	        $('.featured-bar strong').css('color', to );
	       
	    });
	}); 
	
	// submenu color
	/*
    wp.customize( 'fullby_submenu_color', function( value ) {
        value.bind( function( to ) {
            $('.navbar-sub').css('background-color', to );
        });
    });
    */
	
	
          
});