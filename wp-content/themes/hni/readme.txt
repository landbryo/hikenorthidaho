HI! THANKS FOR DOWNLOADING FULLBY 1.6! "Touch Slider"
Responsive Grid Wordpress Theme by MarchettiDesign.net

THEME INSTALLATION
To install and configure the theme see the documentation:
http://www.marchettidesign.net/fullby/docs/

CHANGELOG: 

1.6.5 "Big Grid" add 2 Headers and Big Image Style

1.6.4 add hide menu and sidebar

1.6.3 add full page layout

1.6.2 fix some bugs 

1.6.1 add Infinite Scroll and Footer Widgets, fix Horizontal Widget Bar javascript bug 

1.6 add Touch Slider

1.5.8 add logo responsive

1.5.7 change image size

1.5.6 fix bug ie gallery

1.5.5 add languages po file

1.5.4 Woocommerce Support, Fixed Grid, Related Posts, Rss Feed with images, Max With on single page

1.5.3 Polylang Support

1.5.1 Fix Widget bar bug on grid header

1.5 "Material Color" fix some css bug, and:

	- add one featured color in color system

	- add 6 skin (BlueRed, MagentaYellow, GreenPink, YellowCian, RedBlue, BlackGreen)
	- add horizontal widget bar	
	- add 2 widget (last 3 post from category, last one post form category)	
	- add the video slider 
	- add one font roboto 
	- add custom height header
	- add css for calendar widget

1.0.2 fix Sidebar Mobile and Vimeo Mobile bug

1.0.1 fix DsStore bug