<?php
/*
Plugin Name: 	FULLBY Functionality Plugin
Description: 	All of the important functionality of FULLBY theme.
Text Domain:    fullby-functionality-plugin
Version: 		1.1
License: 		GPL
Author: 		Andrea Marchetti
Author URI: 	http://www.marchettidesign.net
*/

?>
<?php
/* ------------------------------------------------------------------------- *
 *  Make plugin available for translation
/* ------------------------------------------------------------------------- */		


function nec_init() {
	load_plugin_textdomain('fullby-functionality-plugin', false, dirname( plugin_basename( __FILE__ ) ) . '/languages'  );
}
add_action('plugins_loaded', 'nec_init');

?>
<?php // METABOX POST (Video,[...])

/**
 * Adds a box to the main column on the Post and Page edit screens.
 */
 
function fb_video_add_custom_box() {

    $screens = array( 
    			'post', 						// The post type you want this to show up on, can be post, page, or custom post type
	            'normal', 						// The placement of your meta box, can be normal or side
	            'high' );						// The priority in which this will be displayed

    foreach ( $screens as $screen ) {

        add_meta_box(
            	'meta-box-post', 				// ID, should be a string
	            'Youtube & Vimeo - Video', 		// Meta Box Title
	            'fb_video_inner_custom_box', 	// Your call back function, this is where your form field will go
            $screen
        );
    }
}
add_action( 'add_meta_boxes', 'fb_video_add_custom_box' );

/**
 * Prints the box content.
 * 
 * @param WP_Post $post The object for the current post/page.
 */
function fb_video_inner_custom_box( $post ) {

  // Add an nonce field so we can check for it later.
  wp_nonce_field( 'fb_video_inner_custom_box', 'fb_video_inner_custom_box_nonce' );

  /*
   * Use get_post_meta() to retrieve an existing value
   * from the database and use the value for the form.
   */
 
	 $fullby_video = get_post_meta($post->ID, 'fullby_video', true);  
	
	?>

		<p><?php _e('To show a video in the article paste the link of a <strong>YouTube</strong> or <strong>Vimeo</strong> video in the box below.', 'fullby-functionality-plugin');?> <br/><input name="fullby_video" id="fullby_video" value="<?php echo $fullby_video; ?>" style="border: 1px solid #ccc; margin: 10px 10px 0 0; width:100%"/> </p>
	    <p style="border-top:1px solid #eee; padding-top:10px"><?php _e('To set an <strong>Image as a Preview</strong> of the video use the <strong>Featured Image</strong> oh the right.', 'fullby-functionality-plugin'); ?></p>
	      
  <?php

}

/**
 * When the post is saved, saves our custom data.
 *
 * @param int $post_id The ID of the post being saved.
 */
function fb_video_save_postdata( $post_id ) {

  /*
   * We need to verify this came from the our screen and with proper authorization,
   * because save_post can be triggered at other times.
   */

  // Check if our nonce is set.
  if ( ! isset( $_POST['fb_video_inner_custom_box_nonce'] ) )
    return $post_id;

  $nonce = $_POST['fb_video_inner_custom_box_nonce'];

  // Verify that the nonce is valid.
  if ( ! wp_verify_nonce( $nonce, 'fb_video_inner_custom_box' ) )
      return $post_id;

  // If this is an autosave, our form has not been submitted, so we don't want to do anything.
  if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
      return $post_id;

  // Check the user's permissions.
  if ( 'fb_video' == $_POST['post_type'] ) {

    if ( ! current_user_can( 'edit_page', $post_id ) )
        return $post_id;
  
  } else {

    if ( ! current_user_can( 'edit_post', $post_id ) )
        return $post_id;
  }

  /* OK, its safe for us to save the data now. */

  // Sanitize user input.
  $mydata = sanitize_text_field( $_POST['fullby_video'] );
 

  // Update the meta field in the database.
  update_post_meta( $post_id, 'fullby_video', $mydata );

     
}
add_action( 'save_post', 'fb_video_save_postdata' );
?>
<?php // VIDEO FUNCTIONS

function video_image_small($url){ //display url image small youtube and vimeo
		
		$image_url = parse_url($url);
		
		$host = ( ! empty( $image_url['host'] ) ) ? $image_url['host'] : '';
		
		if($host == 'www.youtube.com' || $host == 'youtube.com'){
		
			$array = explode("&", $image_url['query']);
			return "http://img.youtube.com/vi/".substr($array[0], 2)."/0.jpg";
			
		} else if($host == 'www.vimeo.com' || $host == 'vimeo.com'){
		
			$hash = unserialize(file_get_contents("http://vimeo.com/api/v2/video/".substr($image_url['path'], 1).".php"));
			return $hash[0]["thumbnail_small"];
			
		} else {
			
			return "http://img.youtube.com/vi/".$url."/0.jpg";
			
		}
}

function video_image_big($url){ //display url image big youtube and vimeo
		
		$image_url = parse_url($url);
		
		$host = ( ! empty( $image_url['host'] ) ) ? $image_url['host'] : '';
		
		if($host == 'www.youtube.com' || $host == 'youtube.com'){
		
			$array = explode("&", $image_url['query']);
			return "http://img.youtube.com/vi/".substr($array[0], 2)."/hqdefault.jpg";
			
		} else if($host == 'www.vimeo.com' || $host == 'vimeo.com'){
		
			$hash = unserialize(file_get_contents("http://vimeo.com/api/v2/video/".substr($image_url['path'], 1).".php"));
			return $hash[0]["thumbnail_large"];
			
		} else {
			
			return "http://img.youtube.com/vi/".$url."/hqdefault.jpg";
			
		}
}

function video_image_header($url){ //display img tag with class for youtube and vimeo
		
		$image_url = parse_url($url);
		
		$host = ( ! empty( $image_url['host'] ) ) ? $image_url['host'] : '';
		
		if($host == 'www.youtube.com' || $host == 'youtube.com'){
		
			$array = explode("&", $image_url['query']);
			return "<img class='yt-featured' src='http://img.youtube.com/vi/".substr($array[0], 2)."/hqdefault.jpg' class='grid-cop'/>";
			
		} else if($host == 'www.vimeo.com' || $host == 'vimeo.com'){
		
			$hash = unserialize(file_get_contents("http://vimeo.com/api/v2/video/".substr($image_url['path'], 1).".php"));
			return "<img class='vm-featured' src='".$hash[0]["thumbnail_large"]."' class='grid-cop'/>";
			
		} else {
			
			return "<img class='yt-featured' src='http://img.youtube.com/vi/". $url ."/hqdefault.jpg' class='grid-cop'/>";
			
		}
}

function video_player($url){ //display iframe of youtube and vimeo
		
		$image_url = parse_url($url);
		
		$host = ( ! empty( $image_url['host'] ) ) ? $image_url['host'] : '';
		
		if($host == 'www.youtube.com' || $host == 'youtube.com'){
			
			$array = explode("&", $image_url['query']);
			return "<iframe title='YouTube video player' width='400' height='275' src='http://www.youtube.com/embed/".substr($array[0], 2)."?wmode=opaque' frameborder='0'  allowfullscreen></iframe>";
			
		} else if($host == 'www.vimeo.com' || $host == 'vimeo.com'){
			
			return "<iframe src='http://player.vimeo.com/video/".substr($image_url['path'], 1)."' width='400' height='275' frameborder='0' allowfullscreen'></iframe>";
		
		} else {
			
			return "<iframe title='YouTube video player' width='400' height='275' src='http://www.youtube.com/embed/".$url."?wmode=opaque&rel=0' frameborder='0'  allowfullscreen></iframe>";

		}
}

function video_player_autoplay($url){ //display iframe of youtube and vimeo with autoplay
		
		$image_url = parse_url($url);
		
		$host = ( ! empty( $image_url['host'] ) ) ? $image_url['host'] : '';
		
		if($host == 'www.youtube.com' || $host == 'youtube.com'){
		
			$array = explode("&", $image_url['query']);
			return "<iframe title='YouTube video player' width='400' height='275' src='http://www.youtube.com/embed/".substr($array[0], 2)."?wmode=opaque&autoplay=1&rel=0' frameborder='0' wmode='opaque' allowfullscreen></iframe>";
			
		} else if($host == 'www.vimeo.com' || $host == 'vimeo.com'){
			
			return "<iframe src='http://player.vimeo.com/video/".substr($image_url['path'], 1)."?autoplay=true' width='400' height='275' frameborder='0' allowfullscreen'></iframe>";
		
		} else {
			
			return "<iframe title='YouTube video player' width='400' height='275' src='http://www.youtube.com/embed/". $url ."?wmode=opaque&autoplay=1&rel=0' frameborder='0'  allowfullscreen></iframe>";
	
		}
}

?>
<?php // POPULAR POST 

if ( !function_exists('wpb_set_post_views') ) {

function wpb_set_post_views($postID) {
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}
//To keep the count accurate, lets get rid of prefetching
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);

/* add post views to single page */
function wpb_track_post_views ($post_id) {
    if ( !is_single() ) return;
    if ( empty ( $post_id) ) {
        global $post;
        $post_id = $post->ID;    
    }
    wpb_set_post_views($post_id);
}
add_action( 'wp_head', 'wpb_track_post_views');

}

?>
<?php 

// POPULAR AND LATEST POST WIDGET
 
class Pop_Widget extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'pop_widget', // Base ID
			__('Popular and Latest Posts', 'fullby-functionality-plugin'), // Name
			array( 'description' => __( 'Display popular and latest post in a user friendly tab with thumbnail.', 'fullby-functionality-plugin' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		$title = apply_filters( 'widget_title', $instance['title'] );

		
		if ( ! empty( $title ) )
			echo $args['before_title'] . $title . $args['after_title'];
		
		?>
		
		<div class="tab-spacer">

		<!-- Nav tabs -->
		<ul class="nav nav-tabs" id="myTab">
		
			<li class="active"><a href="#home" data-toggle="tab"> <i class="fa fa-bolt"></i> <?php _e('Popular', 'fullby-functionality-plugin'); ?></a></li>
			<li><a href="#profile" data-toggle="tab"> <i class="fa fa-clock-o"></i> <?php _e('Latest', 'fullby-functionality-plugin'); ?></a></li>
			
		</ul>
			
		<!-- Tab panes -->
		<div class="tab-content">
			
			<div class="tab-pane fade in active" id="home">
	
				<?php // POPULAR POST
				
				$popularpost = new WP_Query( array( 'posts_per_page' => 4, 'meta_key' => 'wpb_post_views_count', 'orderby' => 'meta_value_num', 'order' => 'DESC'  ) );
				while ( $popularpost->have_posts() ) : $popularpost->the_post();?>
		
				<a href="<?php the_permalink(); ?>">
				
				
					<?php  // video preview image
					
					global $post;
					
					$video = get_post_meta($post->ID, 'fullby_video', true );

					if ( has_post_thumbnail() ) { ?>
						
						 <?php the_post_thumbnail('thumbnail', array('class' => 'thumbnail')); ?>
					
					<?php } else if ($video != '') { 
						
							// if function video is enable
							if( function_exists('video_image_small')) {
					?>
						
						<img src="<?php echo video_image_small($video); ?>" class="grid-cop"/>
					
					<?php }
					
					} ?>
					                
	
		    		<h3 class="title"><?php the_title(); ?></h3>
		    		
		    		<div class="date"><i class="fa fa-clock-o"></i> <?php the_time('j M , Y') ?> &nbsp;
		    		
		    		
						<?php // video icon
		
						if(($video != '')) { ?>
		             			
		             		<i class="fa fa-video-camera"></i> Video
		             			
		             	<?php } else if (strpos($post->post_content,'[gallery') !== false) { ?>
		             			
		             		<i class="fa fa-th"></i> Gallery
	
	             		<?php } else { } ?>
	             		
	
		    		</div>
	
		    	</a>
		
				<?php endwhile; ?>
			
			</div>
			
			<div class="tab-pane fade" id="profile">
			  	
		  		<?php 
				$popularpost = new WP_Query( array( 'posts_per_page' => 4) );
				while ( $popularpost->have_posts() ) : $popularpost->the_post();?>
		
					<a href="<?php the_permalink(); ?>">
					
					
						<?php  // video preview image
					
						$video = get_post_meta($post->ID, 'fullby_video', true );
	
						if ( has_post_thumbnail() ) { ?>
							
							 <?php the_post_thumbnail('thumbnail', array('class' => 'thumbnail')); ?>
						
						<?php } else if ($video != '') { 
						
							// if function video is enable
							if( function_exists('video_image_small')) {
						?>
							
							<img src="<?php echo video_image_small($video); ?>" class="grid-cop"/>
						
						<?php }
						
						} ?>
						             
		
			    		<h3 class="title"><?php the_title(); ?></h3>
			    		
			    		<div class="date"><i class="fa fa-clock-o"></i> <?php the_time('j M , Y') ?> &nbsp;
			    		
			    		
							<?php // video icon
			
							if(($video != '')) { ?>
			             			
			             		<i class="fa fa-video-camera"></i> Video
			             			
			             	<?php } else if (strpos($post->post_content,'[gallery') !== false) { ?>
			             			
			             		<i class="fa fa-th"></i> Gallery
		
		             		<?php } else { } ?>
		             		
		
			    		</div>
			    		
			    	</a>
		
				<?php endwhile; ?>
			  	
			</div>
					 
		</div>
	
	</div>

	<?php
		
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		}
		else {
			$title = __( ' ', 'fullby' );
		}
		?>
		<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' , 'fullby-functionality-plugin'); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<?php 
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

		return $instance;
	}

} // class Pop_Widget


// register Pop_Widget widget
function register_pop_widget() {
    register_widget( 'Pop_Widget' );
}
add_action( 'widgets_init', 'register_pop_widget' );

?>
<?php

// 3 LATEST POST WIDGET
 
class Last3_Widget extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'Last3', // Base ID
			__('Last 3 posts', 'fullby-functionality-plugin'), // Name
			array( 'description' => __( 'Display latest 3 post from custom category.', 'fullby-functionality-plugin' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
	
		$title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : '';
		
		$cat_sel = ( ! empty( $instance['cat_sel'] ) ) ? $instance['cat_sel'] : '';
        
		// these are the widget options
		$title = apply_filters('widget_title', $instance['title']);
		
		echo $args['before_widget'];
		
		$cat_sel = $instance['cat_sel'];
		
		?>

    			<p class="featured-item"><?php echo $title;?></p>
    			
    			<?php 
				$specialPosts = new WP_Query();
				$specialPosts->query('cat='. $cat_sel .'&showposts=3');
				?>
				
				<?php if ($specialPosts->have_posts()) : while($specialPosts->have_posts()) : $specialPosts->the_post(); ?>
					
					<a href="<?php the_permalink(); ?>">
				
						<?php  // video preview image
						
						global $post;
						
						$video = get_post_meta($post->ID, 'fullby_video', true );
	
						if ( has_post_thumbnail() ) { ?>
							
							 <?php the_post_thumbnail('thumbnail', array('class' => 'thumbnail')); ?>
						
						<?php } else if ($video != '') { 
							
								// if function video is enable
								if( function_exists('video_image_small')) {
						?>
							
							<img src="<?php echo video_image_small($video); ?>" class="grid-cop"/>
						
						<?php }
						
						} ?>
						                
		
			    		<h3 class="title"><?php the_title(); ?></h3>
			    		
			    		<div class="date"><i class="fa fa-clock-o"></i> <?php the_time('j M , Y') ?> &nbsp;
			    		
			    		
							<?php // video icon
			
							if(($video != '')) { ?>
			             			
			             		<i class="fa fa-video-camera"></i> Video
			             			
			             	<?php } else if (strpos($post->post_content,'[gallery') !== false) { ?>
			             			
			             		<i class="fa fa-th"></i> Gallery
		
		             		<?php } else { } ?>
		             		
		
			    		</div>
		
			    	</a>
					
				<?php endwhile;  else :  endif; wp_reset_query();?>	

	<?php
	
	echo $args['after_widget'];
		
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
	
		// Check values 
	if( $instance) { 
	     $title = esc_attr($instance['title']); 
	     $cat_sel = esc_attr($instance['cat_sel']); 
	     
	} else { 
	     $title = ''; 
	     $cat_sel = ''; 

	} 
	?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', 'wp_widget_plugin'); ?></label><br/>
			<input style="width:100%"id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
		</p>
		
		<p><?php _e('Choose a category to show:', 'fullby-functionality-plugin'); ?>
            <select id="<?php echo $this->get_field_id('cat_sel'); ?>" name="<?php echo $this->get_field_name('cat_sel'); ?>" class="widefat" style="width:100%;">
                <?php foreach(get_terms('category') as $term) { ?>
                <option <?php selected($cat_sel, $term->term_id ); ?> value="<?php echo $term->term_id; ?>"><?php echo $term->name; ?></option>
                <?php } ?>      
            </select>
        </p>
			
	<?php 
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['cat_sel'] = ( ! empty( $new_instance['cat_sel'] ) ) ? strip_tags( $new_instance['cat_sel'] ) : '';

		return $instance;
	}

} // class Last3_Widget


// register Last3_Widget widget
function register_Last3_Widget() {
    register_widget( 'Last3_Widget' );
}
add_action( 'widgets_init', 'register_Last3_Widget' );

?>
<?php

// LATEST POST WIDGET
 
class Last1_Widget extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'Last1', // Base ID
			__('Last 1 post', 'fullby-functionality-plugin'), // Name
			array( 'description' => __( 'Display latest post from custom category.', 'fullby-functionality-plugin' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
	
		$title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : '';
		
		$cat_sel = ( ! empty( $instance['cat_sel'] ) ) ? $instance['cat_sel'] : '';
        
		// these are the widget options
		$title = apply_filters('widget_title', $instance['title']);
		
		echo $args['before_widget'];
		
		$cat_sel = $instance['cat_sel'];
		
		?>

    			<p class="featured-item"><?php echo $title;?></p>
    			
    			<?php 
				$specialPosts = new WP_Query();
				$specialPosts->query('cat='. $cat_sel .'&showposts=1');
				?>
				
				<?php if ($specialPosts->have_posts()) : while($specialPosts->have_posts()) : $specialPosts->the_post(); ?>
					
					<a href="<?php the_permalink(); ?>">
				
				
						<?php  // video preview image
						
						global $post;
						
						$video = get_post_meta($post->ID, 'fullby_video', true );
	
						if ( has_post_thumbnail() ) { ?>
							
							 <?php the_post_thumbnail('small-wide', array('class' => 'wg-quad')); ?>
						
						<?php } else if ($video != '') { 
							
								// if function video is enable
								if( function_exists('video_image_small')) {
						?>
							
							<img src="<?php echo video_image_big($video); ?>" class="wg-quad"/>
						
						<?php }
						
						} ?>
						                
		
			    		<h3 class="title"><?php the_title(); ?></h3>
			    		
			    		<div class="date"><i class="fa fa-clock-o"></i> <?php the_time('j M , Y') ?> &nbsp;
			    		
			    		
							<?php // video icon
			
							if(($video != '')) { ?>
			             			
			             		<i class="fa fa-video-camera"></i> Video
			             			
			             	<?php } else if (strpos($post->post_content,'[gallery') !== false) { ?>
			             			
			             		<i class="fa fa-th"></i> Gallery
		
		             		<?php } else { } ?>
		             		
		
			    		</div>
		
			    	</a>
					
				<?php endwhile;  else :  endif;  wp_reset_query(); ?>	
				

	<?php
	
	echo $args['after_widget'];
		
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
	
		// Check values 
	if( $instance) { 
	     $title = esc_attr($instance['title']); 
	     $cat_sel = esc_attr($instance['cat_sel']); 
	     
	} else { 
	     $title = ''; 
	     $cat_sel = ''; 

	} 
	?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', 'fullby-functionality-plugin'); ?></label><br/>
			<input style="width:100%"id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
		</p>
		
		<p><?php _e('Choose a category to show:', 'fullby-functionality-plugin'); ?>
            <select id="<?php echo $this->get_field_id('cat_sel'); ?>" name="<?php echo $this->get_field_name('cat_sel'); ?>" class="widefat" style="width:100%;">
                <?php foreach(get_terms('category') as $term) { ?>
                <option <?php selected($cat_sel, $term->term_id ); ?> value="<?php echo $term->term_id; ?>"><?php echo $term->name; ?></option>
                <?php } ?>      
            </select>
        </p>
		
	<?php 
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['cat_sel'] = ( ! empty( $new_instance['cat_sel'] ) ) ? strip_tags( $new_instance['cat_sel'] ) : '';

		return $instance;
	}

} // class Last1_Widget

// register Last1_Widget widget
function register_Last1_Widget() {
    register_widget( 'Last1_Widget' );
}
add_action( 'widgets_init', 'register_Last1_Widget' );

?>