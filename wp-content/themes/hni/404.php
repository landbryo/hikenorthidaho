<?php get_header(); ?>			
		
	<?php
		
		$main_sidebar_right = get_theme_mod( 'fullby_col_setting', 'option1' );
		$main_sidebar_hide = get_theme_mod( 'fullby_sidebar1_single', '0' ); 
		$second_sidebar_hide = get_theme_mod( 'fullby_sidebar2', '0' ); 
			
	?>		
		
	<div class="<?php if ( $main_sidebar_hide  == '1') { ?> col-md-12 <?php } else { ?> col-md-9 <?php } ?> <?php if ( $main_sidebar_right == 'option1'  && $main_sidebar_hide  != '1') { ?> col-md-push-3 <?php } ?> single">
	
		<div class="<?php if( $second_sidebar_hide == '0') { ?> col-md-9 <?php } else { ?> col-md-12 <?php } ?> no-margin">

            <div class="row spacer-sing"></div>	
            
            <div class="single-in">

				<div class="sing-tit-cont">
					
					<h3 class="sing-tit"><?php _e('404 Page not found', 'fullby'); ?></h3>
				
				</div>
	
				<div class="sing-cont">
					
					<div class="sing-spacer">
					
						<p><?php _e('Try to make a search...', 'fullby'); ?></p>
			
						<form class="search-light" role="search" method="get" action="<?php echo home_url(); ?>">
						    <div class="input-group">
						        <input type="text" placeholder="Search" name="s">
						        
						    </div>
					    </form>
	
					</div>
	
				</div>	
				
			</div><!--/single-in-->
	        
		</div>	
		 
		<?php if( $second_sidebar_hide == '0') { ?>
		 
			<div class="col-md-3">
			
				<div class="sec-sidebar">
	
					<?php get_sidebar( 'secondary' ); ?>	
											
			    </div>
			   
			</div>
		 
		<?php } ?>

	</div>			

	<?php if ( $main_sidebar_hide  != '1') { ?>			

		<div class="col-md-3 <?php if ( $main_sidebar_right == 'option1' ) { ?> col-md-pull-9 <?php } ?> sidebar">
	
			<?php get_sidebar( 'primary' ); ?>	
			    
		</div>
	
	<?php } ?> 
		
<?php get_footer(); ?>