	<?php do_action('scree_pre_footer'); ?>
	<div class="col-md-12 footer">
	
		&copy; Copyright <?php echo date("o");?>  &nbsp;<?php echo get_theme_mod( 'fullby_icon', '<i class="fa fa-chevron-right"></i><i class="fa fa-chevron-right"></i>' ); ?>&nbsp; <span> <?php bloginfo('name'); ?></span> <strong class="pull-right"><a href="#top"><i class="fa fa-angle-double-up"></i> TOP</a></strong>
		
	</div>
	
	
	<?php if ( is_active_sidebar( 'footer-sidebar' ) ) { ?>
		
		<div class="col-md-12 footer-bar">
			
			<?php dynamic_sidebar( 'footer-sidebar' ); ?>
			
		</div>
		
	<?php } ?>

	<?php /* <div class="col-md-12 footer">
		
		<?php echo get_theme_mod( 'fullby_footer', 'Powered by: <a href="http://www.marchettidesign.net">MarchettiDesign</a>' ); ?>
		
	</div> */ ?>
	
	<?php wp_footer();?>
	
    <?php do_action('scree_body_close'); ?>
  </body>
</html>

    	