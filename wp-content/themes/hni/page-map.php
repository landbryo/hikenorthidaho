<?php

/*

Template Name: Map

*/

?>
<?php get_header(); ?>

    <div class="col-md-12 single">

		<?php $hikes = get_posts( array( 'post_type' => 'post', 'posts_per_page' => - 1 ) );
		if ( ! empty( $hikes ) ): ?>

            <div class="acf-map" style="height: 800px;">

				<?php foreach ( $hikes as $hike ): ?>
					<?php
					$location  = get_field( 'hike_location', $hike->ID );
					$title     = get_the_title( $hike->ID );
					$permalink = get_permalink( $hike->ID );
					$thumbnail = get_the_post_thumbnail( $hike->ID, array(
						100,
						100
					), array( 'class' => 'aligncenter' ) );
					$icon      = get_stylesheet_directory_uri() . "/scree/img/mapicon.png";

					if ( ! empty( $location ) ): ?>

                        <div class="marker" data-lat="<?php echo $location['lat']; ?>"
                             data-lng="<?php echo $location['lng']; ?>" data-icon="<?php echo $icon; ?>">
                            <a href="<?php echo $permalink; ?>">
                                <h4><?php echo $title; ?></h4>
								<?php echo $thumbnail; ?>
                            </a>
                        </div>
					<?php endif; ?>
				<?php endforeach; ?>

            </div>

		<?php endif; ?>

    </div>
    <?php /* <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/scree/js/map.js"></script> */ ?>

<?php get_footer(); ?>