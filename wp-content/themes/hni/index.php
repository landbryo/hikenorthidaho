<?php get_header(); ?>	


	<?php 
		/* Retrive options grid */
		$big_image_grid = get_theme_mod( 'fullby_activate_big_grid', '0' ); 
		$main_sidebar_right = get_theme_mod( 'fullby_col_setting', 'option1' );
		$main_sidebar_hide = get_theme_mod( 'fullby_sidebar1', '0' ); 
	?>
		
	
	<div class="<?php if ( $main_sidebar_hide  == '1') { ?> col-md-12 <?php } else { ?> col-md-9 <?php } ?> <?php if ( $main_sidebar_right == 'option1' && $main_sidebar_hide  != '1') { ?> col-md-push-3 <?php } ?> cont-grid <?php if($big_image_grid == '1') { ?> big-images-grid <?php } ?>">
				
		<?php if ( is_search() ) { // if is search display the search key ?>

			<p class="result"><?php _e('Result for:', 'fullby'); ?> <strong><i><?php echo $s ?></i></strong></p>
		
		<?php }  else if ( is_author() ){ ?>
		
			<p class="result"><?php _e('Post by:', 'fullby'); ?> <strong><i><?php the_author(); ?></i></strong></p>
			
		<?php } ?>

		<div class="grid">
		
			<?php // if fixed height selected add class to div "link-video" and div "gra" to the bottom of the item
				
				$post_height = '';
				$full_content = '';
				$excerpt_content = '';
				
				$option_selected = get_theme_mod( 'fullby_post_content', 'option2');
						
				if ( 'option3' == $option_selected  ) { 
				
					$post_height = 'ok'; 
				
				} else if ( 'option2' == $option_selected  ) {
				
					$full_content = 'ok'; 	
					
				} else {
					
					$excerpt_content = 'ok'; 
				
				}

			?>
					
			<?php if (have_posts()) :?><?php while(have_posts()) : the_post(); ?> 

				<div class="item <?php if ($post_height != ''){?> fixed-post-height <?php }?>">
					
					<?php if($big_image_grid == '1') {
							
							get_template_part( 'inc/image-grid' ); 
							
					} ?>
				
					<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

						<p class="grid-cat"><?php the_category(', '); ?></p> 
						
						<h2 class="grid-tit"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
						
						<p class="meta"> <i class="fa fa-clock-o"></i> <?php the_time('j M , Y') ?> &nbsp;
						
						
							<?php // video icon
							
							$video = get_post_meta($post->ID, 'fullby_video', true );
												
							if(($video != '')) { ?>
						 			
						 		<i class="fa fa-video-camera"></i> Video
						 			
						 	<?php } else if (strpos($post->post_content,'[gallery') !== false) { ?>
						 			
						 		<i class="fa fa-th"></i> Gallery
						
							<?php } else {?>
					
							<?php } ?>
							
								
						</p>
						
						<?php if($big_image_grid != '1') {
							
							get_template_part( 'inc/image-grid' ); 
							
						} ?>
						
						

						<div class="grid-text">
													
							<?php if ($full_content != '') { 
							    the_content('More...');
							 } else {
							    the_excerpt();
							 } ?>
							
						</div>
						
						<p>
							<?php $post_tags = wp_get_post_tags($post->ID); if(!empty($post_tags)) {?>
								<span class="tag-post"> <i class="fa fa-tag"></i> <?php the_tags('', ', ', ''); ?> </span>
							<?php } ?>
						</p>
						
					</div>
					
					<?php if ($post_height != ''){?>
						
						<div class="gra"></div>
					
					<?php }?>

				</div>	

			<?php endwhile; ?>
	        <?php else : ?>
	        	
	        	<div class="item">
	        		
	        		<div class="post">

	                	<p><?php _e('Sorry, no posts matched your criteria.', 'fullby'); ?></p>
	                
	                </div>
	        		
	        	</div>

	        <?php endif; ?> 

		</div>	

		<div class="pagination">
		
			<?php
			global $wp_query;
			
			$big = 999999999; // need an unlikely integer
			
			echo paginate_links( array(
				'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
				'format' => '?paged=%#%',
				'current' => max( 1, get_query_var('paged') ),
				'total' => $wp_query->max_num_pages
			) );
			?>
			
		</div>
			
	</div>
	
	<?php if ( $main_sidebar_hide  != '1') { ?>
	
		<div class="col-md-3 <?php if ( $main_sidebar_right == 'option1' ) { ?> col-md-pull-9 <?php } ?> sidebar">
	
			<?php get_sidebar( 'primary' ); ?>		
			    
		</div>
	
	<?php } ?>
	
<?php get_footer(); ?>	