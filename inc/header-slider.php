<?php $tag = get_theme_mod( 'fullby_featured', 'featured' ); ?>

		
	<!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
    
	    
	      <!-- Indicators -->
	      <ol class="carousel-indicators">
	
				<?php $counter = 0;?>        
	
				<?php
				$specialPosts = new WP_Query();
				$specialPosts->query('tag='.$tag.'&showposts=3');
				?>
				
				<?php if ($specialPosts->have_posts()) : while($specialPosts->have_posts()) : $specialPosts->the_post(); ?>
			        
			        <li data-target="#myCarousel" data-slide-to="<?php echo $counter ?>" class="<?php $counter++; if ($counter == 1){ ?>active<?php } ?>"></li>
			        
				<?php endwhile;  else : endif; ?>
				
	      </ol>
	      
	      <div class="carousel-inner">
	     
	     
				<?php $counter = 0;?>        

				<?php // Loop Single Cover 
				
				$specialPosts = new WP_Query();
				$specialPosts->query('tag='.$tag.'&showposts=3');
				
				?>
				
				<?php if ($specialPosts->have_posts()) : while($specialPosts->have_posts()) : $specialPosts->the_post(); ?>
				
				<?php $video = get_post_meta($post->ID, 'fullby_video', true );	?>
				
				
					<?php // Cover Image
	
					// if post thumbnail exist
					
					if ( has_post_thumbnail() ) { ?>
					
	
						<div class="item <?php $counter++; if ($counter == 1){ ?> active <?php } ?>" style="background: url(<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'cover' ); ?>); background-size:100% auto; ">
  	
				             
				             <a href="<?php the_permalink(); ?>" class="link-featured-video">
				             
					             <div class="carousel-caption">
					    	
							    		<div class="cat"><span><?php $category = get_the_category(); echo $category[0]->cat_name; ?></span></div>
				    		
							    		<div class="date-feat"><i class="fa fa-clock-o"></i> <?php the_time('j M , Y') ?> &nbsp;
							    		
							
											<?php if(($video != '')) { ?>
							             			
							             		<i class="fa fa-video-camera"></i> Video
							             			
							             	<?php } else if (strpos($post->post_content,'[gallery') !== false) { ?>
							             			
							             		<i class="fa fa-th"></i> Gallery
							
							         		<?php } else {?>
							
							         		<?php } ?>
							
							    		
							    		</div>
	
							    		
							    		<h2 class="title"><?php the_title(); ?></h2>
							    		
						    	</div>
					    	
				            </a>

			            </div>
			            
			        <?php 
	 
				 	} else { // Cover Video 
					 	
					   // if functionality plug in with function video is enable
					   	
					   if( function_exists('video_player')) {
					 
					?>
					
						<div class="item <?php $counter++; if ($counter == 1){ ?> active <?php } ?>">
						
							<div class="videoWrapper v-home">
			
							 	<div class='video-container'><?php echo video_player($video); ?></div>
							
							</div>
							
							<a href="<?php the_permalink(); ?>" class="link-featured-video">
  	
				             <div class="carousel-caption">
				    	
						    		<div class="cat"><span><?php $category = get_the_category(); echo $category[0]->cat_name; ?></span></div>
						    		
						    		<div class="date-feat"><i class="fa fa-clock-o"></i> <?php the_time('j M , Y') ?> &nbsp; </div>
						    		
						    		<h2 class="title"><?php the_title(); ?></h2>
						    		
					    	</div>
					    	
							</a>

			            </div>

					<?php 
					 	
					 	} //end if video is enable
					 
					 
					 } //end if/else post thumbnail exist 
					 
					 ?>	            
	            

				<?php endwhile;  else : ?>
		
			
					<p><?php _e('Sorry, no posts matched your criteria.', 'fullby'); ?></p>
			
		
				<?php endif; ?>

	      </div>
	      
	      <a class="left carousel-control" href="#myCarousel" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
	      <a class="right carousel-control" href="#myCarousel" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
	      
	      <?php get_sidebar( 'widget-bar' ); ?>
	      
	      
	</div><!-- /.carousel -->
	

<?php // get_template_part( 'inc/featured-bar' ); ?>	