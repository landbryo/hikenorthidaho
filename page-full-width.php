<?php

/*

Template Name: Page Full Width

*/

?>
<?php get_header(); ?>			
		
	<div class="col-md-12 single">
	
		<div class="col-md-12">
		
			<?php if (have_posts()) :?><?php while(have_posts()) : the_post(); ?> 

				<?php if ( has_post_thumbnail() ) { ?>

                    <?php the_post_thumbnail('single', array('class' => 'sing-cop')); ?>

                <?php } else { ?>
                
                	<div class="row spacer-sing"></div>	
                
                 <?php }  ?>
				
				<div class="sing-tit-cont">
					
					<h2 class="sing-tit"><?php the_title(); ?></h2>
				
				</div>

				<div class="sing-cont">
					
					<div class="sing-spacer">
					
						<?php the_content('Leggi...');?>

					</div>

				</div>	
				 					
			<?php endwhile; ?>
	        <?php else : ?>

	                <p><?php _e('Sorry, no posts matched your criteria.', 'fullby'); ?></p>
	         
	        <?php endif; ?> 
	        
		</div>	
		 
		
	</div>			

		
<?php get_footer(); ?>