	<div class="social">
	
		<?php if(get_theme_mod( 'fullby_link_fb' ) != '' ) { ?>
			
			<a target="_blank" rel="nofollow" href="<?php echo get_theme_mod( 'fullby_link_fb' ) ?>"><i class="fa fa-facebook fa-2x"></i></a>
		
		<?php } ?>
		
		<?php if(get_theme_mod( 'fullby_link_tw' ) != '' ) { ?>
			
			<a target="_blank" rel="nofollow" href="<?php echo get_theme_mod( 'fullby_link_tw' ) ?>"><i class="fa fa-twitter fa-2x"></i></a>
		
		<?php } ?>
		
		<?php if(get_theme_mod( 'fullby_link_gp' ) != '' ) { ?>
			
			<a target="_blank" rel="nofollow" href="<?php echo get_theme_mod( 'fullby_link_gp' ) ?>"><i class="fa fa-google-plus fa-2x"></i></a>
		
		<?php } ?>
		
		<?php if(get_theme_mod( 'fullby_link_pn' ) != '' ) { ?>
			
			<a target="_blank" rel="nofollow" href="<?php echo get_theme_mod( 'fullby_link_pn' ) ?>"><i class="fa fa-pinterest fa-2x"></i></a>
		
		<?php } ?>
		
		<?php if(get_theme_mod( 'fullby_link_in' ) != '' ) { ?>
			
			<a target="_blank" rel="nofollow" href="<?php echo get_theme_mod( 'fullby_link_in' ) ?>"><i class="fa fa-instagram fa-2x"></i></a>
		
		<?php } ?>
		
		<?php if(get_theme_mod( 'fullby_link_li' ) != '' ) { ?>
			
			<a target="_blank" rel="nofollow" href="<?php echo get_theme_mod( 'fullby_link_li' ) ?>"><i class="fa fa-linkedin fa-2x"></i></a>
		
		<?php } ?>
		
		<?php if(get_theme_mod( 'fullby_link_yt' ) != '' ) { ?>
			
			<a target="_blank" rel="nofollow" href="<?php echo get_theme_mod( 'fullby_link_yt' ) ?>"><i class="fa fa-youtube fa-2x"></i></a>
		
		<?php } ?>
		
	</div>

	
	<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar(__('Primary Sidebar', 'fullby')) ) : ?>
	
	<?php endif; ?>		