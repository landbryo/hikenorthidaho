<?php // WOO COMMERCE

add_theme_support( 'woocommerce' ); 

?>
<?php // THEME LANGUAGES

load_theme_textdomain( 'fullby', get_template_directory().'/languages' );

?>
<?php // MENU 


	add_action( 'after_setup_theme', 'fbm_setup' );
    if ( ! function_exists( 'fbm_setup' ) ):
        function fbm_setup() { 
            register_nav_menu( 'primary', __( 'Primary navigation', 'fullby' ) );
            register_nav_menu( 'secondary', __( 'Secondary navigation', 'fullby' ) );
    } endif;

    
?>
<?php // BOOTSTRAP MENU - Custom navigation walker (Required)


    require_once('functions/wp_bootstrap_navwalker.php');
    
    
?>
<?php // Scree functions


    require_once('scree/functions.php');


?>
<?php // CUSTOM THUMBNAIL 


	add_theme_support('post-thumbnails');
	
	if ( function_exists('add_theme_support') ) {
		add_theme_support('post-thumbnails');
	}
	
	if ( function_exists( 'add_image_size' ) ) { 
		add_image_size( 'small-wide', 260, 150, true ); 
		add_image_size( 'quad', 500, 500, true );   //(cropped)
		add_image_size( 'single', 700, 432, true ); //(cropped)
		add_image_size( 'cover', 1000, 600, true ); //(cropped)
	}
?>
<?php // WOOCOMMERCE IMAGE SHOP RESIZE
	
	/*
	 * Hook in on activation
	 *
	 */
	add_action( 'init', 'yourtheme_woocommerce_image_dimensions', 1 );
	
	/**
	 * Define image sizes
	 */
	function yourtheme_woocommerce_image_dimensions() {
	  
		$thumbnail = array(
			'width' 	=> '400',	// px
			'height'	=> '400',	// px
			'crop'		=> 1 		// true
		);
	
		// Image sizes
		update_option( 'shop_thumbnail_image_size', $thumbnail ); 	// Image gallery thumbs
	}	

?>
<?php // WIDGET SIDEBAR 


	if ( function_exists('register_sidebar') ) {
		register_sidebar(array('name'=> __('Primary Sidebar', 'fullby'),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',	
			'after_widget' => '</div>',
			'before_title' => '<h3>',
			'after_title' => '</h3>',
		));
		register_sidebar(array('name'=> __('Secondary Sidebar', 'fullby'),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',	
			'after_widget' => '</div>',
			'before_title' => '<h3>',
			'after_title' => '</h3>',
		));
		register_sidebar(array('name'=> __('Horizontal Widget Bar', 'fullby'),
			'before_widget' => '<div id="%1$s" class="box-widget %2$s"><div class="box-widget-inside">',	
			'after_widget' => '</div></div>',
			'before_title' => '<p class="featured-item title-black">',
			'after_title' => '</p>',
		));
		register_sidebar(array('name'=> __('Woocommmerce Sidebar', 'fullby'),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',	
			'after_widget' => '</div>',
			'before_title' => '<h3>',
			'after_title' => '</h3>',

		));
		register_sidebar(array('name'=> __('Footer Sidebar', 'fullby'),
			'id' => 'footer-sidebar',
			'before_widget' => '<div class="col-md-3"><div id="%1$s" class="widget %2$s">',	 
			'after_widget' => '</div></div>',
			'before_title' => '<p class="featured-item title-black">',
			'after_title' => '</p>',		
		));
	}
	

?>
<?php // INCLUDE SCRIPT 

	
if ( ! function_exists( 'fb_scripts' ) ) {
	
	function fb_scripts() {


		wp_register_script( 'bootstrap-js', get_template_directory_uri().'/js/bootstrap.min.js', array( 'jquery' ),null,true );
		wp_enqueue_script( 'bootstrap-js' );
		
		
		if ( !is_singular() ) { 
			wp_register_script( 'isotope-js', get_template_directory_uri().'/js/isotope.js', array( 'jquery' ),null,true );
			wp_enqueue_script( 'isotope-js' );
			
			 
			/* infinite scroll script*/
			
			if ( '' != get_theme_mod( 'fullby_activate_infinitescroll', '')) {
				
				wp_enqueue_script( 'infinitescroll', get_template_directory_uri() . '/js/jquery.infinitescroll.min.js', array( 'jquery' ),null, true ); 
				
				wp_enqueue_script( 'fullby-grid-infinite', get_template_directory_uri().'/js/fullby-grid-infinite.js', array(), '1.0.0', true );
				
				$array = array(
					"path_loader" => get_template_directory_uri().'/img/loader.gif',
				);
	
			    wp_localize_script( "fullby-grid-infinite", "php_vars", $array );
				
			} else {
				
				wp_register_script( 'fullby-grid-js', get_template_directory_uri().'/js/fullby-grid.js', array( 'jquery' ),null,true );
				wp_enqueue_script( 'fullby-grid-js' );
				
			}			
			
		}
		
		if ( is_home() ) { 
		
			wp_register_script( 'fullby-parallax-js', get_template_directory_uri().'/js/fullby-parallax.js', array( 'jquery' ),null,true );
			wp_enqueue_script( 'fullby-parallax-js' );
			
			wp_enqueue_script( 'slick', get_template_directory_uri() . '/js/slick.min.js', array( 'jquery' ),null, true );
		
		}

		if ( is_singular() ) { 
			wp_enqueue_script( 'sharrre', get_template_directory_uri() . '/js/jquery.sharrre.min.js', array( 'jquery' ),null, true ); 
		}
		
		wp_enqueue_script( 'magnific-popup', get_template_directory_uri() . '/js/jquery.magnific-popup.min.js', array( 'jquery' ),null, true );
		
		
		if ( 'option5' == get_theme_mod( 'fullby_cover_content', 'option5') && is_home()) {

			 
		    wp_enqueue_script( "slick-options", get_template_directory_uri() . "/js/slick-options.js", array(), '1.0.0', true );
		    
		    $array = array(
				"autoplay" => get_theme_mod( 'fullby_touch_slider', '1' ) == '0' ? 'false' : 'true',
			    "pause_hover" => get_theme_mod( 'fullby_touch_slider_pause', '0' ) == '0' ? 'false' : 'true',
			    "autoplay_speed" => get_theme_mod( 'fullby_touch_slider_speed', '5000' )
			  );

		    wp_localize_script( "slick-options", "php_vars", $array );
		    
		}
		
		
		wp_register_script( 'fullby-script-js', get_template_directory_uri().'/js/fullby-script.js', array( 'jquery' ),null,true );
		wp_enqueue_script( 'fullby-script-js' );
	
	}  
	
}
add_action( 'wp_enqueue_scripts', 'fb_scripts' );


?>
<?php // THEME OPTIONS


function fullby_theme_customizer( $wp_customize ) {



	/* ICON TEXT LOGO
	--------------------------------------------------------------------------- */

	$wp_customize->add_setting( 'fullby_icon', array(
		'default'	=> '<i class="fa fa-chevron-right"></i><i class="fa fa-chevron-right"></i>',
	) );
	
	$wp_customize->add_control( 'fullby_icon', array(
		'label'	=> __( 'Icon - Paste it from Font Awesome', 'fullby' ),
		'section'	=> 'title_tagline',
		'settings'	=> 'fullby_icon',
		'type'	=> 'text',
	
	) );
	
	
	
	/* LOGO, FAVICON & HEADER
	--------------------------------------------------------------------------- */
	
		
    $wp_customize->add_section( 'fullby_logo_section' , array(
		'title' => __( 'Logo, Favicon & Header', 'fullby' ),
		'priority' => 25,
		'description' => __( 'Upload a logo to replace the default site name and description in the header', 'fullby' ),
	) );
	
	
	// Logo uploader
	
	$wp_customize->add_setting( 'fullby_logo', array(
		'default'	=> '',
	) );

	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'fullby_logo', array(
		'label'	=> __( 'Logo', 'fullby' ),
		'section'	=> 'fullby_logo_section',
		'settings'	=> 'fullby_logo',
		
	) ) );
	
	
	// Favicon uploader
	
	$wp_customize->add_setting( 'fullby_favicon', array(
		'default'	=> '',
	) );
	
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'fullby_favicon', array(
		'label'	=> __( 'Favicon 16 x 16 px .png', 'fullby' ),
		'section'	=> 'fullby_logo_section',
		'settings'	=> 'fullby_favicon',
		
	) ) );
	
	
	// Header height
		
	$wp_customize->add_setting( 'fullby_header_height', array(
		'default'	=> 'option1',
	) );
	
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'fullby_header_height', array(
		'label'	=> __( 'Header Height', 'fullby' ),
		'section'	=> 'fullby_logo_section',
		'settings'	=> 'fullby_header_height',
		'type'	=> 'radio',
		'choices'	=> array(
		'option1'	=> 'Small - 50px',
		'option2'	=> 'Medium - 70px',
		'option3'	=> 'Big - 100px',
	),
	) ) );


    
    /* COLORS
	--------------------------------------------------------------------------- */
    
    
    // Header Color 
    
    $wp_customize->add_setting(
    'fullby_header_color',
	    array(
	        'default'     => '#00ebbe',
	        'transport'   => 'postMessage'
	    )
	);
 
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'fullby_header_color', array(
        'label'	=> __('Header Color', 'fullby'),
        'section' => 'colors',
        'settings' => 'fullby_header_color',
        'priority' => 2, 
    ) ) );
    
    
    // Link Color 
    
    $wp_customize->add_setting(
    'fullby_link_color',
	    array(
	        'default'     => '#02c29d',
	        'transport'   => 'postMessage'
	    )
	);
 
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'fullby_link_color', array(
        'label'	=> __('Link Color', 'fullby'),
        'section' => 'colors',
        'settings' => 'fullby_link_color',
        'priority' => 4, 
    ) ) );
    
    
    // Link Hover Color 
    
    $wp_customize->add_setting(
    'fullby_hover_color',
	    array(
	        'default'     => '#ff0052',
	        /*'transport'   => 'postMessage'*/
	    )
	);
 
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'fullby_hover_color', array(
        'label'	=> __('Link Hover Color', 'fullby'),
        'section' => 'colors',
        'settings' => 'fullby_hover_color',
        'priority' => 6, 
    ) ) );	
    
    
    // Logo & Menu Color 
    
    $wp_customize->add_setting(
    'fullby_logo_menu_color',
	    array(
	        'default'     => '#000000',
	        'transport'   => 'postMessage'
	    )
	);
 
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'fullby_logo_menu_color', array(
        'label'	=> __('Logo & Menu Link Color', 'fullby'),
        'section' => 'colors',
        'settings' => 'fullby_logo_menu_color',
        'priority' => 8, 
    ) ) );	
    
    
    // Featured Color 
    
    $wp_customize->add_setting(
    'fullby_featured_color',
	    array(
	        'default'     => '#04debc',
	        'transport'   => 'postMessage'
	    )
	);
 
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'fullby_featured_color', array(
        'label'	=> __('Featured Color', 'fullby'),
        'section' => 'colors',
        'settings' => 'fullby_featured_color',
        'priority' => 10, 
    ) ) );	
    
    
    // Sub Menu Color
    
    $wp_customize->add_setting(
    'fullby_submenu_color',
	    array(
	        'default'     => '#dedbcd',
	        'transport'   => 'postMessage'
	    )
	);
 
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'fullby_submenu_color', array(
        'label'	=> __('Sub Menu Color', 'fullby'),
        'section' => 'colors',
        'settings' => 'fullby_submenu_color',
        'priority' => 11, 
    ) ) );	
    
    
    // Sub Menu Over Color
    
    $wp_customize->add_setting(
    'fullby_submenu_over_color',
	    array(
	        'default'     => '#c7c1ab',
	        /*'transport'   => 'postMessage'*/
	    )
	);
 
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'fullby_submenu_over_color', array(
        'label'	=> __('Sub Menu Hover Color', 'fullby'),
        'section' => 'colors',
        'settings' => 'fullby_submenu_over_color',
        'priority' => 12, 
    ) ) );	
    
    
    // Sub Menu Link Color
    
    $wp_customize->add_setting(
    'fullby_submenu_link_color',
	    array(
	        'default'     => '#666666',
	        /*'transport'   => 'postMessage'*/
	    )
	);
 
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'fullby_submenu_link_color', array(
        'label'	=> __('Sub Menu Link Color', 'fullby'),
        'section' => 'colors',
        'settings' => 'fullby_submenu_link_color',
        'priority' => 13, 
    ) ) );	
	
	
	// Skin System 
	 
	$wp_customize->add_setting( 'fullby_skin', array(
		'default'	=> 'option1',
	) );
	
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'fullby_skin', array(
		'label'	=> __( 'Skins', 'fullby' ),
		'section'	=> 'colors',
		'settings'	=> 'fullby_skin',
		'priority' => 15, 
		'type'	=> 'radio',
		'choices'	=> array(
		'option1'	=> 'Default',
		'option2'	=> 'Skin 1',
		'option3'	=> 'Skin 2',
		'option4'	=> 'Skin 3',
		'option5'	=> 'Skin 4',
		'option6'	=> 'Skin 5',
		'option7'	=> 'Skin 6',
		'option8'	=> 'Skin 7 (Dark)',
		'option9'	=> 'Skin 8 (Light)',
		'option10'	=> 'Skin 9 (Jungle)',
		'option11'	=> 'Skin 10 (Flower)',
		'option12'	=> 'Skin 11 (Lime)',
		'option13'	=> 'Skin 12 (Mango)',
	),
	) ) );
    
    
    
	/* GOOGLE FONT
	--------------------------------------------------------------------------- */
    
    $wp_customize->add_section( 'fullby_font_section' , array(
		'title' => __( 'Font', 'fullby' ),
		'priority' => 40,
		'description' => __( 'Select Font for Logo, Title and Menu.', 'fullby' ),
	) );
    
    
	   $wp_customize->add_setting( 'fullby_font_setting', array(
	        'default' => 'font1',
	    ) );
	    
	   $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'fullby_font_setting', array(
	        'label' => __( 'Google Font Setting', 'fullby' ),
	        'section' => 'fullby_font_section',
	        'settings' => 'fullby_font_setting',
	        'type'	=> 'select',
			'choices'	=> array(
				'font1'	=> 'Lato',
				'font2'	=> 'Raleway',
				'font3'	=> 'Source Sans Pro',
				'font4'	=> 'Oswald',
				'font5'	=> 'Oxygen',
				'font6'	=> 'Lobster',
				'font7'	=> 'Dosis',
				'font8'	=> 'Arvo',
				'font9'	=> 'Muli',
				'font10'=> 'Droid Sans',
				'font11'=> 'Roboto',
		),
	       
	    ) ) );
	    
	    
	    // Text Uppercase or Lowercase
	    
	    $wp_customize->add_setting( 'fullby_text_setting', array(
	        'default' => 'option1',
	    ) );	
	    
	    $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'fullby_text_setting', array(
	        'label' => __( 'Letter Case Setting', 'fullby' ),
	        'section' => 'fullby_font_section',
	        'settings' => 'fullby_text_setting',
	        'type'	=> 'radio',
			'choices'	=> array(
				'option1'	=> 'Uppercase',
				'option2'	=> 'Capitalize',
		),
	       
	    ) ) );	
	


    /* DISPLAY OPTION
	--------------------------------------------------------------------------- */

    $wp_customize->add_section( 'fullby_layout_section' , array(
		'title' => __( 'Layout Options', 'fullby' ),
		'priority' => 42,
		'description' => __( 'Change how Fullby displays Header Covers, Posts and Sidebars', 'fullby' ),
	) );
	
		$wp_customize->add_setting( 'fullby_cover_content', array(
			'default'	=> 'option0',
		) );
		
		
		// Cover
		
		$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'fullby_cover_content', array(
			'label'	=> __( 'Header Style', 'fullby' ),
			'section'	=> 'fullby_layout_section',
			'settings'	=> 'fullby_cover_content',
			'type'	=> 'radio',
			'choices'	=> array(
			'option0'	=> __( 'No Header Display', 'fullby' ),
			'option6'	=> __( 'Single Image + List', 'fullby' ),
			'option1'	=> __( 'Single Parallax Image / Video', 'fullby' ),
			'option2'	=> __( 'Single Parallax Image / Video Autoplay', 'fullby' ),
			'option3'	=> __( '3 columns Grid', 'fullby' ),
			'option4'	=> __( 'Slider Image / Video', 'fullby' ),
			'option5'	=> __( '4 Columns Touch Slider', 'fullby' ),
			'option7'	=> __( '4 Box Grid', 'fullby' ),
		),
		) ) );
		
		// Touch Slider
		
		$wp_customize->add_setting( 'fullby_touch_slider', array(
			'default'	=> 1,
		) );
		
		$wp_customize->add_control( 'fullby_touch_slider', array(
			'label'	=> __( 'Autoplay Touch Slider', 'fullby' ),
			'section'	=> 'fullby_layout_section',
			'settings'	=> 'fullby_touch_slider',
			'type'	=> 'checkbox',
			'priority' => 50, 
			
		) );
		
		$wp_customize->add_setting( 'fullby_touch_slider_pause', array(
			'default'	=> 0,
		) );
		
		$wp_customize->add_control( 'fullby_touch_slider_pause', array(
			'label'	=> __( 'Pause on Hover Touch Slider', 'fullby' ),
			'section'	=> 'fullby_layout_section',
			'settings'	=> 'fullby_touch_slider_pause',
			'type'	=> 'checkbox',
			'priority' => 55, 
			
		) );
		
		
		$wp_customize->add_setting( 'fullby_touch_slider_speed', array(
				'default'	=> '5000',
			) );
			
			$wp_customize->add_control( 'fullby_touch_slider_speed', array(
				'label'	=> __( 'Autoplay Speed (milliseconds)', 'fullby' ),
				'section'	=> 'fullby_layout_section',
				'settings'	=> 'fullby_touch_slider_speed',
				'type'	=> 'text',
				'priority' => 65,
			
			) );
			
			
		// Infinite Scroll
		
		$wp_customize->add_setting( 'fullby_activate_infinitescroll', array(
			'default'	=> 0,
		) );
		
		$wp_customize->add_control( 'fullby_activate_infinitescroll', array(
			'label'	=> __( 'Activate Infinite Scroll', 'fullby' ),
			'section'	=> 'fullby_layout_section',
			'settings'	=> 'fullby_activate_infinitescroll',
			'type'	=> 'checkbox',
			'priority' => 85, 
			
		) );
		
		
		// Big Image Grid
		
		$wp_customize->add_setting( 'fullby_activate_big_grid', array(
			'default'	=> 0,
		) );
		
		$wp_customize->add_control( 'fullby_activate_big_grid', array(
			'label'	=> __( 'Activate Big Images Grid & Layout', 'fullby' ),
			'section'	=> 'fullby_layout_section',
			'settings'	=> 'fullby_activate_big_grid',
			'type'	=> 'checkbox',
			'priority' => 87, 
			
		) );
		
		
		// Horizontal Widget Bar
		
		$wp_customize->add_setting( 'fullby_show_widget_bar', array(
			'default'	=> 0,
		) );
		
		$wp_customize->add_control( 'fullby_show_widget_bar', array(
			'label'	=> __( 'Show Horizontal Widget Bar in Home', 'fullby' ),
			'section'	=> 'fullby_layout_section',
			'settings'	=> 'fullby_show_widget_bar',
			'type'	=> 'checkbox',
			'priority' => 90, 
			
		) );
		
		
		// Related Post
		
		$wp_customize->add_setting( 'fullby_related_posts', array(
			'default'	=> 0,
		) );
		
		$wp_customize->add_control( 'fullby_related_posts', array(
			'label'	=> __( 'Hide Related Posts in Single Page', 'fullby' ),
			'section'	=> 'fullby_layout_section',
			'settings'	=> 'fullby_related_posts',
			'type'	=> 'checkbox',
			'priority' => 96, 
			
		) );
		
		// Author 
		
		$wp_customize->add_setting( 'fullby_author', array(
			'default'	=> 0,
		) );
		
		$wp_customize->add_control( 'fullby_author', array(
			'label'	=> __( 'Hide Author on Single Page', 'fullby' ),
			'section'	=> 'fullby_layout_section',
			'settings'	=> 'fullby_author',
			'type'	=> 'checkbox',
			'priority' => 95, 
			
		) );

		
		// Sidebar Position
		
		$wp_customize->add_setting( 'fullby_col_setting', array(
			'default'	=> 'option1',
		) );
		
		$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'fullby_col_setting', array(
			'label'	=> __( 'Sidebar Position', 'fullby' ),
			'section'	=> 'fullby_layout_section',
			'settings'	=> 'fullby_col_setting',
			'priority' => 88, 
			'type'	=> 'radio',
			'choices'	=> array(
			'option1'	=> __( 'Sidebar Left', 'fullby' ),
			'option2'	=> __( 'Sidebar Right', 'fullby' ),
		),
		) ) );
		
		
		// Main Sidebar
		
		$wp_customize->add_setting( 'fullby_sidebar1', array(
			'default'	=> 0,
		) );
		
		$wp_customize->add_control( 'fullby_sidebar1', array(
			'label'	=> __( 'Hide Primary Sidebar in Home', 'fullby' ),
			'section'	=> 'fullby_layout_section',
			'settings'	=> 'fullby_sidebar1',
			'type'	=> 'checkbox',
			'priority' => 91, 
			
		 ) );
		 
		// Main Sidebar in Singles
		
		$wp_customize->add_setting( 'fullby_sidebar1_single', array(
			'default'	=> 0,
		) );
		
		$wp_customize->add_control( 'fullby_sidebar1_single', array(
			'label'	=> __( 'Hide Primary Sidebar in Internal Pages', 'fullby' ),
			'section'	=> 'fullby_layout_section',
			'settings'	=> 'fullby_sidebar1_single',
			'type'	=> 'checkbox',
			'priority' => 92, 
			
		 ) );

		
		// Second Sidebar
		
		$wp_customize->add_setting( 'fullby_sidebar2', array(
			'default'	=> 0,
		) );
		
		$wp_customize->add_control( 'fullby_sidebar2', array(
			'label'	=> __( 'Hide Secondary Sidebar', 'fullby' ),
			'section'	=> 'fullby_layout_section',
			'settings'	=> 'fullby_sidebar2',
			'type'	=> 'checkbox',
			'priority' => 93, 
			
		 ) );
		 
		 // Woocommerce Sidebar
		
		$wp_customize->add_setting( 'fullby_sidebar_wc', array(
			'default'	=> 0,
		) );
		
		$wp_customize->add_control( 'fullby_sidebar_wc', array(
			'label'	=> __( 'Hide Woocommerce Sidebar', 'fullby' ),
			'section'	=> 'fullby_layout_section',
			'settings'	=> 'fullby_sidebar_wc',
			'type'	=> 'checkbox',
			'priority' => 94, 
			
		 ) );

		 
		 
		 // Second Menu
		
		$wp_customize->add_setting( 'fullby_menu2', array(
			'default'	=> 0,
		) );
		
		$wp_customize->add_control( 'fullby_menu2', array(
			'label'	=> __( 'Hide Second Menu', 'fullby' ),
			'section'	=> 'fullby_layout_section',
			'settings'	=> 'fullby_menu2',
			'type'	=> 'checkbox',
			'priority' => 95, 
			
		 ) );



		// Excerpt
		
		$wp_customize->add_setting( 'fullby_post_content', array(
			'default'	=> 'option2',
		) );
		
		$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'fullby_post_content', array(
			'label'	=> __( 'Post Content', 'fullby' ),
			'section'	=> 'fullby_layout_section',
			'settings'	=> 'fullby_post_content',
			'priority' => 70, 
			'type'	=> 'radio',
			'choices'	=> array(
			'option1'	=> __( 'Excerpts', 'fullby' ),
			'option2'	=> __( 'Full content', 'fullby' ),
			'option3'	=> __( 'Fixed height', 'fullby' ),
		),
		) ) );
		
		
		
	/* FEATURED
	--------------------------------------------------------------------------- */

    $wp_customize->add_section( 'fullby_featured_section' , array(
		'title' => __( 'Featured Tag', 'fullby' ),
		'priority' => 43,
		'description' => __( 'Use the "featured" tag to feature your posts in the header. You can change this to a tag of your choice;', 'fullby' ),
	) );
		
		$wp_customize->add_setting( 'fullby_featured', array(
			'default'	=> 'featured',
		) );
		
		$wp_customize->add_control( 'fullby_featured', array(
			'label'	=> __( 'Tag use for Header Posts', 'fullby' ),
			'section'	=> 'fullby_featured_section',
			'settings'	=> 'fullby_featured',
			'type'	=> 'text',
		
		) );
		
	
	
	/* SOCIAL
	--------------------------------------------------------------------------- */
	
    $wp_customize->add_section( 'fullby_social_section' , array(
		'title' => __( 'Social', 'fullby' ),
		'priority' => 45,
		'description' => __( 'Change how Fullby display Social Elements', 'fullby' ),
	) );
	
	$wp_customize->add_setting( 'fullby_share_content', array(
		'default'	=> 1,
	) );
	
	$wp_customize->add_control( 'fullby_share_content', array(
		'label'	=> __( 'Show share buttons on single page', 'fullby' ),
		'section'	=> 'fullby_social_section',
		'settings'	=> 'fullby_share_content',
		'type'	=> 'checkbox',
		'priority' => 1, 
		
	 ) );
	 
			 // Facebook
			 
			$wp_customize->add_setting( 'fullby_link_fb', array(
				'default'	=> '',
			) );
			
			$wp_customize->add_control( 'fullby_link_fb', array(
				'label'	=> __( 'Facebook Link', 'fullby' ),
				'section'	=> 'fullby_social_section',
				'settings'	=> 'fullby_link_fb',
				'type'	=> 'text',
			
			) );
			
			
			// Twitter
			
			$wp_customize->add_setting( 'fullby_link_tw', array(
				'default'	=> '',
			) );
			
			$wp_customize->add_control( 'fullby_link_tw', array(
				'label'	=> __( 'Twitter Link', 'fullby' ),
				'section'	=> 'fullby_social_section',
				'settings'	=> 'fullby_link_tw',
				'type'	=> 'text',
			
			) );
			
			
			// Google +
			
			$wp_customize->add_setting( 'fullby_link_gp', array(
				'default'	=> '',
			) );
			
			$wp_customize->add_control( 'fullby_link_gp', array(
				'label'	=> __( 'Google+ Link', 'fullby' ),
				'section'	=> 'fullby_social_section',
				'settings'	=> 'fullby_link_gp',
				'type'	=> 'text',
			
			) );
			
			
			// Pinterest 
			
			$wp_customize->add_setting( 'fullby_link_pn', array(
				'default'	=> '',
			) );
			
			$wp_customize->add_control( 'fullby_link_pn', array(
				'label'	=> __( 'Pinterest Link', 'fullby' ),
				'section'	=> 'fullby_social_section',
				'settings'	=> 'fullby_link_pn',
				'type'	=> 'text',
			
			) );
			
			
			// Instagram
			 
			$wp_customize->add_setting( 'fullby_link_in', array(
				'default'	=> '',
			) );
			
			$wp_customize->add_control( 'fullby_link_in', array(
				'label'	=> __( 'Instagram Link', 'fullby' ),
				'section'	=> 'fullby_social_section',
				'settings'	=> 'fullby_link_in',
				'type'	=> 'text',
			
			) );
			
			
			// Youtube 
			
			$wp_customize->add_setting( 'fullby_link_yt', array(
				'default'	=> '',
			) );
			
			$wp_customize->add_control( 'fullby_link_yt', array(
				'label'	=> __( 'YouTube Link', 'fullby' ),
				'section'	=> 'fullby_social_section',
				'settings'	=> 'fullby_link_yt',
				'type'	=> 'text',
			
			) );
			
			
			// Linkedin
			
			$wp_customize->add_setting( 'fullby_link_li', array(
				'default'	=> '',
			) );
			
			$wp_customize->add_control( 'fullby_link_li', array(
				'label'	=> __( 'Linkedin Link', 'fullby' ),
				'section'	=> 'fullby_social_section',
				'settings'	=> 'fullby_link_li',
				'type'	=> 'text',
			
			) );
	
		
		
	/* SEO
	--------------------------------------------------------------------------- */

    $wp_customize->add_section( 'fullby_seo_section' , array(
		'title' => __( 'SEO', 'fullby' ),
		'priority' => 50,
		'description' => __( 'Write a Meta Description. To activate Google Analytics insert Analytics ID code and the site domain without www (ex. mysite.com).', 'fullby' ),
	) );
		
		
		// Meta description 
		
		$wp_customize->add_setting( 'fullby_metadesc', array(
			'default'	=> '',
		) );
		
		$wp_customize->add_control( 'fullby_metadesc', array(
			'label'	=> __( 'Meta Description', 'fullby' ),
			'section'	=> 'fullby_seo_section',
			'settings'	=> 'fullby_metadesc',
			'type'	=> 'text',
		
		) );
		
		
		// Analytics
		
		$wp_customize->add_setting( 'fullby_analytics', array(
			'default'	=> '',
		) );
		
		$wp_customize->add_control( 'fullby_analytics', array(
			'label'	=> __( 'Analytics ID', 'fullby' ),
			'section'	=> 'fullby_seo_section',
			'settings'	=> 'fullby_analytics',
			'type'	=> 'text',
		
		) );
		
		$wp_customize->add_setting( 'fullby_analytics_dom', array(
			'default'	=> '',
		) );
		
		$wp_customize->add_control( 'fullby_analytics_dom', array(
			'label'	=> __( 'Site Domain (without www)', 'fullby' ),
			'section'	=> 'fullby_seo_section',
			'settings'	=> 'fullby_analytics_dom',
			'type'	=> 'text',
		
		) );
		
		
	/* Footer
	--------------------------------------------------------------------------- */

    $wp_customize->add_section( 'fullby_footer_section' , array(
		'title' => __( 'Footer', 'fullby' ),
		'priority' => 50,
		'description' => __( 'Write a message in the footer.', 'fullby' ),
	) );
		
		
		// Footer text
		
		$wp_customize->add_setting( 'fullby_footer', array(
			'default'	=> 'Powered by: <a href="http://www.marchettidesign.net">MarchettiDesign</a>',
		) );
		
		$wp_customize->add_control( 'fullby_footer', array(
			'label'	=> __( 'Footer Message', 'fullby' ),
			'section'	=> 'fullby_footer_section',
			'settings'	=> 'fullby_footer',
			'type'	=> 'text',
		
		) );



	// Set site name and description to be previewed in real-time
	$wp_customize->get_setting('blogname')->transport='postMessage';
	$wp_customize->get_setting('blogdescription')->transport='postMessage';
	
	// Remove nav selector
	$wp_customize->remove_section( 'nav');

	  
	// Enqueue scripts for real-time preview
	wp_enqueue_script( 'fullby-customizer', get_template_directory_uri() . '/js/fullby-customizer.js', array( 'jquery' ) );
	 
 
}
add_action('customize_register', 'fullby_theme_customizer');

// CSS customizer chenages

function fullby_customizer_css() {
    ?>
    
    <?php 
    
    	// Header Height
    	
    	$header_height  = get_theme_mod( 'fullby_header_height', 'option2');
    	$header_height_px = '';

    	// Defaults
        $submenu_color      = '';
        $submenu_over_color = '';
	    $submenu_link_color = '';
    	
    	if ( 'option1' == $header_height ) {  
    	
    		// nothing
    	
    	} else if ( 'option2' == $header_height ) {
    	
    		$header_height_px = '70';
    	
    	} else if ( 'option3' == $header_height ) {
    	
    		$header_height_px = '100';
    	
    	}
    
    
    	// Customizer Color + Skin

	    $skin_selected  = get_theme_mod( 'fullby_skin' , 'option1'); 
	    
	    $payoff_color = '';


	 	if ( 'option1' == $skin_selected ) { 
	 		
	 		// Default
	 		
	    	$header_color 	 = get_theme_mod( 'fullby_header_color', '#00ebbe' ); 
		    $link_color  	 = get_theme_mod( 'fullby_link_color', '#02c29d' );
		    $hover_color 	 = get_theme_mod( 'fullby_hover_color', '#ff0052' ); 
		    $logo_menu_color = get_theme_mod( 'fullby_logo_menu_color', '#000000' );
		    $featured_color	 = get_theme_mod( 'fullby_featured_color', '#04debc' );
		    $submenu_color	 = get_theme_mod( 'fullby_submenu_color', '#dedbcd' );
		    $submenu_over_color	 = get_theme_mod( 'fullby_submenu_over_color', '#c7c1ab' );
		    $submenu_link_color	 = get_theme_mod( 'fullby_submenu_link_color', '#666' );
		    
		    		     	 	 		
    	} else if ( 'option2' == $skin_selected  ) {
	    	
	    	// Skin 1
	    	
	    	$header_color 	 = '#03a8f6'; 
		    $link_color  	 = '#fc4f56';
		    $hover_color 	 = '#0388d6';
		    $logo_menu_color = '#00558b';
		    $featured_color	 = '#fc4f56';
		    

    	} else if ( 'option3' == $skin_selected  ) {
	    	
	    	// Skin 2
	    	
		    $header_color 	 = '#f2005b'; 
		    $link_color  	 = '#8d0f52';
		    $hover_color 	 = '#fed52a';
		    $logo_menu_color = '#740d3e';
		    $featured_color	 = '#fed52a';
	    	

    	} else if ( 'option4' == $skin_selected  ) {
	    	
	    	// Skin 3
	    	
	    	$header_color 	 = '#009688'; 
		    $link_color  	 = '#04e0be';
		    $hover_color 	 = '#f60059';
		    $logo_menu_color = '#03fffc';
		    $featured_color	 = '#f60059';
		    
		    
		} else if ( 'option5' == $skin_selected  ) {
		    
		    // Skin 4
		    
		    $header_color 	 = '#ffe63c'; 
		    $link_color  	 = '#f20260';
		    $hover_color 	 = '#04debc';
		    $logo_menu_color = '#645a16';
		    $featured_color	 = '#04debc';


	    } else if ( 'option6' == $skin_selected  ) {
		   
		    // Skin 5
		    
		    $header_color 	 = '#f5463b'; 
		    $link_color  	 = '#01bdd5';
		    $hover_color 	 = '#05d7f2';
		    $logo_menu_color = '#800d26';
		    $featured_color	 = '#05d7f2';
		    
		    
		} else if ( 'option7' == $skin_selected  ) {
		    
		    // Skin 6
		    
		    $header_color 	 = '#333333'; 
		    $link_color  	 = '#01cba4';
		    $hover_color 	 = '#05d7f2';
		    $logo_menu_color = '#00ebbe';
		    $featured_color	 = '#00ebbe';
		    

    	} else if ( 'option8' == $skin_selected  ) {
		    
		    // Skin 7
		    
		    $header_color 	 = '#36474f'; 
		    $link_color  	 = '#536e7b';
		    $hover_color 	 = '#00e5ff';
		    $logo_menu_color = '#ffffff';
		    $featured_color	 = '#77909d';
		    $payoff_color	 = '#b0bec5';
		    

    	} else if ( 'option9' == $skin_selected  ) {
		    
		    // Skin 8
		    
		    $header_color 	 = '#f7f7f7'; 
		    $link_color  	 = '#7a7a7a';
		    $hover_color 	 = '#ff5454';
		    $logo_menu_color = '#6f6f6f';
		    $featured_color	 = '#94999c';
		    $payoff_color	 = '#cccccc';
		    

    	} else if ( 'option10' == $skin_selected  ) {
		    
		    // Skin 9
		    
		    $header_color 	 = '#44d86b'; 
		    $link_color  	 = '#b096e0';
		    $hover_color 	 = '#ff1f5e';
		    $logo_menu_color = '#ffffff';
		    $featured_color	 = '#ff1f5e';
		    $payoff_color	 = '#b3fdc6';


    	} else if ( 'option11' == $skin_selected  ) {
		    
		    // Skin 10
		    
		    $header_color 	 = '#f7f7f7'; 
		    $link_color  	 = '#ff6660';
		    $hover_color 	 = '#ff5454';
		    $logo_menu_color = '#ff6660';
		    $featured_color	 = '#ff6660';
		    $payoff_color	 = '#96a8b3';
		    

    	} else if ( 'option12' == $skin_selected  ) {
		    
		    // Skin 10
		    
		    $header_color 	 = '#2ffdb9'; 
		    $link_color  	 = '#5bcbf6';
		    $hover_color 	 = '#1eb4f1';
		    $logo_menu_color = '#333';
		    $featured_color	 = '#1eb5f1';
		    $payoff_color	 = '#127eba';
		    

    	} else if ( 'option13' == $skin_selected  ) {
		    
		    // Skin 10
		    
		    $header_color 	 = '#fade3e'; 
		    $link_color  	 = '#f46144';
		    $hover_color 	 = '#e5401f';
		    $logo_menu_color = '#333';
		    $featured_color	 = '#f46144';
		    $payoff_color	 = '#f46144';
		    

    	}						
    	
    ?>
    
    <style type="text/css">

        #mainmenu, 
        .navbar-fixed-top,
        #comments #submit,
        .single-image-list .date-feat,
        .big-images-grid .grid-cat,
        .big-images-grid-single .cat,
        .grid-box .date-feat {background-color: <?php echo $header_color; ?>; }
        
        .navbar-sub{background-color: <?php echo $submenu_color; ?>; }
        .navbar-sub .navbar-nav > li > a:hover{ background-color: <?php echo $submenu_over_color; ?> }
        .navbar-sub .navbar-nav > li > a{color: <?php echo $submenu_link_color; ?>; }
        
        
        .featured .date-feat,
        .item-featured-single .date-feat,
        .carousel-caption .date-feat,
        .featured-item,
        .list-article-header a span {color: <?php echo $featured_color; ?>}
        
        .widget-bar-button{background-color: <?php echo $featured_color; ?>}
        
        .tag-post{color: <?php echo $header_color; ?>}
             
        a{color: <?php echo $link_color; ?> }
        
        .navbar-fixed-top .navbar-nav > li > a:hover{background-color: <?php echo $link_color; ?>}

        a:focus, a:hover, 
        .grid-tit a:hover {color:<?php echo $hover_color; ?>}  
        
        .navbar-fixed-top .navbar-nav > li > a, 
        .navbar-fixed-top .navbar-brand,
        .single-image-list .date-feat,
        .big-images-grid .grid-cat a,
        .big-images-grid-single .cat a,
        .grid-box .date-feat {color: <?php echo $logo_menu_color; ?>;}
        
        
        <?php if( get_theme_mod( 'fullby_menu2', '0' ) == '1') { ?> 
        
        	.navbar-sub{min-height: 0px;}
        
        <?php } ?>
        
        <?php if($payoff_color != '') { ?>
	        
	          .navbar small { color: <?php echo $payoff_color; ?>;}
	        
        <?php } ?>

        <?php if ( 'option1' != $header_height ) { ?>
        
	        @media (min-width: 1000px) { 
	        	
	        	.navbar-fixed-top .navbar-brand,
				.navbar-fixed-top .navbar-nav li a {
				    line-height: <?php echo $header_height_px; ?>px;
				    height: <?php echo $header_height_px; ?>px;
				    padding-top: 0;
				}
				
				.navbar-header h1 {line-height: <?php echo $header_height_px - 10; ?>px}
				
				#cover:hover~.featured-bar, 
				.featured-bar:hover, 
				.link-featured-single:hover~.featured-bar{top:<?php echo $header_height_px; ?>px}
				
				body.admin-bar .featured-bar{top: <?php echo $header_height_px + 2; ?>px;}
	
				body.admin-bar #cover:hover~.featured-bar, 
				body.admin-bar .featured-bar:hover {top: <?php echo $header_height_px + 32; ?>px;}
				
				.spacer,
				.spacer-4-grid-box{height: <?php echo $header_height_px; ?>px;}
				
				.featured{min-height: <?php echo $header_height_px; ?>px}
				
			}

        <?php } ?>        
        
        <?php /* Set in CSS the Google Font Selected */
        	
        	$font_setting = get_theme_mod( 'fullby_font_setting', 'font1');
        	$text_setting = get_theme_mod( 'fullby_text_setting', 'option1' ); 
	        
	        $font_selected = 'Lato';
	        
	        if ( 'font2' ==  $font_setting ) { 
	        
	        	$font_selected = 'Raleway';
	        
	        } else if ( 'font3' == $font_setting ) {
		        
		        $font_selected = 'Source Sans Pro';
	        
	        } else if ( 'font4' == $font_setting ) {
		        
		        $font_selected = 'Oswald';
	        
	        } else if ( 'font5' == $font_setting ) {
		        
		        $font_selected = 'Oxygen';
	        
	        } else if ( 'font6' == $font_setting ) {
		        
		        $font_selected = 'Lobster';
	        
	        } else if ( 'font7' == $font_setting ) {
		        
		        $font_selected = 'Dosis';
	        
	        } else if ( 'font8' == $font_setting ) {
		        
		        $font_selected = 'Arvo';
	        
	        } else if ( 'font9' == $font_setting ) {
		        
		        $font_selected = 'Muli';
		        
	        } else if ( 'font10' == $font_setting ) {
		        
		        $font_selected = 'Droid Sans';
	        
	        } else if ( 'font11' == $font_setting ) {
		        
		        $font_selected = 'Roboto';
	        }
	        
	        // Font and Lettercase Setting
			
			if ('font1' != $font_setting ){ ?>

				.title, 
				.grid-tit, 
				.sing-tit,
				.widget h3,
				.navbar-inverse .navbar-brand span,
				.navbar-inverse .navbar-nav > li > a  {font-family: <?php echo $font_selected ?>; font-weight: 400; <?php if ( 'option2' == $text_setting ) { echo 'text-transform:capitalize!important'; } ?> }
				
				 <?php if ($font_selected == 'Lobster') { ?>
					
					.navbar-inverse .navbar-brand span{ letter-spacing: 0px;}
					.widget h3{font-size:18px;}
					
				<?php } ?>

			<?php  } ?>

    </style>
    
    <?php
}
add_action( 'wp_head', 'fullby_customizer_css' );


?>
<?php // INCLUDE STYLE


if ( ! function_exists( 'fb_style' ) ) {
	
	function fb_style() {

		// all style
		
		wp_register_style( 'bootstrap', get_stylesheet_directory_uri().'/css/bootstrap.min.css', '', '', 'screen' );
		wp_enqueue_style( 'bootstrap' );
		
		wp_register_style( 'font-awesome', get_stylesheet_directory_uri().'/font-awesome/css/font-awesome.min.css', '', null, 'screen' );
		wp_enqueue_style( 'font-awesome' );
		
		wp_register_style( 'screen', get_stylesheet_directory_uri().'/style.css', '', null, 'screen' );
		wp_enqueue_style( 'screen' );
		
		wp_register_style( 'magnific-popup', get_stylesheet_directory_uri().'/css/magnific-popup.css', '', '', 'screen' );
		wp_enqueue_style( 'magnific-popup' );
		
		wp_register_style( 'slick', get_stylesheet_directory_uri().'/css/slick.css', '', '', 'screen' );
		wp_enqueue_style( 'slick' );
		
		wp_register_style( 'lato', '//fonts.googleapis.com/css?family=Lato:300,400,700,900', '', '', 'screen' );
		wp_enqueue_style( 'lato' );	
		
		
		/* Embed selected Google Font */
		
		$fb_font = 'Lato';
		$fb_font_selected = get_theme_mod( 'fullby_font_setting', 'font1');
		
		if ( 'font2' == $fb_font_selected ){
		
			$fb_font = 'Raleway';
						    
		} else if ( 'font3' == $fb_font_selected ){
			
			$fb_font = 'Source+Sans+Pro';
		
		} else if ( 'font4' == $fb_font_selected ){
			
			$fb_font = 'Oswald';
		
		} else if ( 'font5' == $fb_font_selected ){
			
			$fb_font = 'Oxygen';
		
		} else if ( 'font6' == $fb_font_selected ){
			
			$fb_font = 'Lobster';
		
		} else if ( 'font7' == $fb_font_selected ){
			
			$fb_font = 'Dosis';
		
		} else if ( 'font8' == $fb_font_selected ){
			
			$fb_font = 'Arvo';
		
		} else if ( 'font9' == $fb_font_selected ){
			
			$fb_font = 'Muli';
		
		} else if ( 'font10' == $fb_font_selected ){
			
			$fb_font = 'Droid+Sans';
			
		} else if ( 'font11' == $fb_font_selected ){
			
			$fb_font = 'Roboto';
			
		}

		if ('font1' != $fb_font_selected ){
		
			wp_register_style( 'google-font', 'http://fonts.googleapis.com/css?family='.$fb_font.':300,400,700', '', '', 'screen' );
			wp_enqueue_style( 'google-font' );	
		
		}	
		
	}  
	
}
add_action( 'wp_enqueue_scripts', 'fb_style' );


?>
<?php // AUTHOR Social Info
	
	function add_twitter_contactmethod( $contactmethods ) {
	
	$contactmethods['twitter'] = 'Twitter (Full URL)';
	$contactmethods['facebook'] = 'Facebook (Full URL)';
	$contactmethods['googleplus'] = 'Google+ (Full URL)';
	$contactmethods['linkedin'] = 'LinkedIn (Full URL)';
	$contactmethods['youtube'] = 'YouTube (Full URL)';
	
	return $contactmethods;
}
add_filter('user_contactmethods','add_twitter_contactmethod',10,1);

?>
<?php // CONTENT WIDTH & feedlinks
 

	if ( ! isset( $content_width ) ) $content_width = 900;
	add_theme_support( 'automatic-feed-links' );
	

?>
<?php // REPLY comment script 


	function fullby_enqueue_comments_reply() {
		if( get_option( 'thread_comments' ) )  {
			wp_enqueue_script( 'comment-reply' );
		}
	}
	add_action( 'comment_form_before', 'fullby_enqueue_comments_reply' );
	
	
?>
<?php // RSS image in feed

function rss_post_thumbnail($content) {
	
	global $post;
	
	if(has_post_thumbnail($post->ID)) {
	
		$content = '<p>' . get_the_post_thumbnail($post->ID, 'single') . '</p>' . get_the_content();
		$content = preg_replace("/\[caption.*\[\/caption\]/", '',$content);
		
	}
	
	return $content;
}
add_filter('the_excerpt_rss', 'rss_post_thumbnail');
add_filter('the_content_feed', 'rss_post_thumbnail');

?>
<?php
/**
 * Include the TGM_Plugin_Activation class.
 */
require_once dirname( __FILE__ ) . '/functions/class-tgm-plugin-activation.php';


/*  TGM plugin activation
/* ------------------------------------ */
if ( ! function_exists( 'my_theme_register_required_plugins' ) ) {
	
	function my_theme_register_required_plugins() {
		
		// Add the following plugins
		$plugins = array(
			array(
				'name' 				=> 'FULLBY Functionality Plugin',
				'slug' 				=> 'fullby-functionality-plugin',
				'source'			=> get_template_directory() . '/functions/fullby-functionality-plugin.zip',
				'required'			=> true,
				'force_activation' 	=> false,
				'force_deactivation'=> false,
			)
		);	
		tgmpa( $plugins );
	}
	
}

add_action( 'tgmpa_register', 'my_theme_register_required_plugins' );

?>